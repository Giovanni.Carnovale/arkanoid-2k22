#pragma once

#include "SystemHeaders.h"
#include "resource.h"
#include "Keyboard.h"
#include "Mouse.h"
#include "Graphics.h"

#include <string>
#include <optional>
#include <memory>
#include <exception>

class WindowException : public std::exception
{
public:
	WindowException(int line, const char* file, HRESULT hr) noexcept;
	char const* what() const override;
	static std::string TranslateErrorCode(HRESULT hr) noexcept;

private:
	
	HRESULT hr;
	int line;
	std::string file;
	mutable std::string whatBuffer;
};

#define WND_EXCEPT(hr) WindowException(__LINE__, __FILE__, hr);
#define WND_LASTEXCEPT() WindowException(__LINE__, __FILE__, GetLastError());


class Window
{
private:

class WindowClass {
	public:
		static const char* GetName() noexcept;
		static HINSTANCE GetInstance() noexcept;
	private:
		WindowClass() noexcept;
		~WindowClass();
		WindowClass( const WindowClass& ) = delete;
		WindowClass& operator= ( const WindowClass& ) = delete;

		static constexpr const char* wndClassName = "Arkanoid 2k22";
		static WindowClass Instance;
		HINSTANCE hInst;
};

public:
	Window(int width, int height, const char* name);
	~Window();
	Window(const Window&) = delete;
	Window& operator=(const Window&) = delete;
	void SetTitle(const std::string& title);
	void EnableCursor() noexcept;
	void DisableCursor() noexcept;
	bool CursorEnabled() const noexcept;
	static std::optional<int> ProcessMessages();
	Graphics& GetGraphics();

	Keyboard keyboard;
	Mouse mouse;

private:

	static LRESULT CALLBACK HandleMsgSetup(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) noexcept;
	static LRESULT CALLBACK HandleMsgThunk(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) noexcept;
	LRESULT HandleMsg(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) noexcept;
	int width;
	int height;
	HWND hWnd;
	std::unique_ptr<Graphics> pGraphics;
};

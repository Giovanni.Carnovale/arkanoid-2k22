#pragma once
#include <queue>
#include <utility>

class Mouse
{
	friend class Window;
public:
	class Event
	{
	public:
		enum class Type
		{
			LPRESS, LRELEASE,
			RPRESS, RRELEASE,
			WHEELUP, WHEELDOWN,
			ENTER, LEAVE,
			MOVE, INVALID
		};

		inline Event() noexcept : type(Type::INVALID) {}
		inline Event(Type type, const Mouse& Parent) noexcept : 
			type(type), isLeftPressed(Parent.isLeftPressed), 
			isRightPressed(Parent.isRightPressed), x(Parent.x), y(Parent.y) {}

		inline Type GetType() const noexcept
		{
			return type;
		}
		inline std::pair<int, int> GetPos() const noexcept
		{
			return{ x,y };
		}
		inline int GetPosX() const noexcept
		{
			return x;
		}
		inline int GetPosY() const noexcept
		{
			return y;
		}
		inline bool IsLeftPressed() const noexcept
		{
			return isLeftPressed;
		}
		inline bool IsRightPressed() const noexcept
		{
			return isRightPressed;
		}

	private:
		Type type;
		bool isLeftPressed = false, isRightPressed = false;
		int x = 0, y = 0;

	};

	Mouse() = default;
	Mouse( const Mouse& ) = delete;
	Mouse& operator= ( const Mouse& ) = delete;
	inline std::pair<int, int> GetPos() const noexcept;
	inline int GetPosX() const noexcept { return x; };
	inline int GetPosY() const noexcept { return y; };
	inline bool IsInWindow() const noexcept { return isInWindow; };
	inline bool IsLeftPressed() const noexcept { return isLeftPressed; };
	inline bool IsRightPressed() const noexcept { return isRightPressed; };
	Mouse::Event Read() noexcept;
	inline bool IsEmpty() const noexcept { return buffer.empty(); }
	void Flush() noexcept;
private:
	void OnMouseMove( int x, int y ) noexcept;
	void OnLeftPressed( int x, int y ) noexcept;
	void OnLeftReleased( int x, int y ) noexcept;
	void OnRightPressed(int x, int y) noexcept;
	void OnRightReleased(int x, int y) noexcept;
	void OnWheelUp(int x, int y) noexcept;
	void OnWheelDown(int x, int y) noexcept;
	void OnMouseEnter() noexcept;
	void OnMouseLeave() noexcept;
	void OnWheelDelta(int x, int y, int delta) noexcept;
	void TrimBuffer() noexcept;

	static constexpr unsigned int bufferSize = 16u;
	int x, y;
	bool isLeftPressed = false, isRightPressed = false, isInWindow = false;
	int wheelDeltaCarry = 0;
	std::queue<Event> buffer;
};


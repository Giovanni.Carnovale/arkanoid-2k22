#pragma once

#include "SystemHeaders.h"

#include <d3d11.h>
#include <exception>
#include <wrl.h>
#include <string>

#include <DirectXMath.h>
#include "Camera.h"

class Graphics
{
friend class Bindable; //Children of Bindable will not be friend

public:
	Graphics(HWND hWnd);
	Graphics(const Graphics&) = delete;
	Graphics& operator=(const Graphics&) = delete;
	~Graphics() = default;

	void EndFrame();
	void ClearBuffer( float red, float green, float blue) noexcept;
	
	//Camera accessors
	inline void SetCameraTransform(const DirectX::XMFLOAT3& pos, const DirectX::XMFLOAT3& rot) { camera.SetCamera(pos, rot);}
	inline DirectX::FXMMATRIX GetCameraTransform() const noexcept { return camera.GetTransform(); }
	inline DirectX::FXMMATRIX GetViewTransform() const noexcept { return camera.GetInverseTransform(); }
	inline DirectX::XMFLOAT3 GetCameraPosition() const noexcept { return camera.GetPosition(); };
	inline DirectX::XMFLOAT3 GetCameraRotation() const noexcept { return camera.GetRotation(); };


	inline DirectX::XMMATRIX GetProjection() const {
		constexpr float ar = 9.f / 16.f;
		return DirectX::XMMatrixPerspectiveLH(1.0, ar, 0.5f, 50.f);
	}
	void DrawIndexed(UINT count) const noexcept;
	void DrawTestTriangle(float x, float y, float angle);
private:
	Microsoft::WRL::ComPtr<ID3D11Device> pDevice = nullptr;
	Microsoft::WRL::ComPtr <IDXGISwapChain> pSwap = nullptr;
	Microsoft::WRL::ComPtr <ID3D11DeviceContext> pContext = nullptr;
	Microsoft::WRL::ComPtr <ID3D11RenderTargetView> pTarget = nullptr;
	Microsoft::WRL::ComPtr <ID3D11DepthStencilView> pDepthStencilView = nullptr;

	Camera camera;
};


#pragma once


namespace GameInfo
{
	static constexpr float LeftLimit = -20.f, RightLimit = -LeftLimit,
		TopLimit = 12.f, BottomLimit = -11.f;
}

#pragma once
#include "Drawable/DrawableBase.h"
#include "Surface.h"
#include <string>

class PlanarTexture : public DrawableBase<PlanarTexture>
{
public:
	PlanarTexture(Graphics& gfx, const DirectX::XMFLOAT3& position, const DirectX::XMFLOAT3& rotation, const DirectX::XMFLOAT3& scale, const std::wstring& texturepath);
	PlanarTexture(Graphics& gfx, const DirectX::XMFLOAT3& position,
		const DirectX::XMFLOAT3& velocity, const DirectX::XMFLOAT3& rotation,
		const DirectX::XMFLOAT3& angularvelocity, const DirectX::XMFLOAT3& scale, const std::wstring& texturepath);

	DirectX::XMMATRIX GetTransformXM() const noexcept override;
	void Update(float dt) noexcept override;

private:
	Surface TextureSurface;
	
	DirectX::XMFLOAT3 position;
	DirectX::XMFLOAT3 velocity;
	DirectX::XMFLOAT3 rotation;
	DirectX::XMFLOAT3 angularvelocity;
	DirectX::XMFLOAT3 scale;

};
#pragma once

#include "Drawable/DrawableBase.h"

#include<DirectXMath.h>

class LoadedModel : public DrawableBase<LoadedModel>
{
public:
	LoadedModel(Graphics& gfx, const DirectX::XMFLOAT3& position, const DirectX::XMFLOAT3& rotation, const DirectX::XMFLOAT3& scale, const DirectX::XMFLOAT3& materialColor, const std::string& Path);
	LoadedModel(Graphics& gfx, const DirectX::XMFLOAT3& position,
		const DirectX::XMFLOAT3& velocity, const DirectX::XMFLOAT3& rotation,
		const DirectX::XMFLOAT3& angularvelocity, const DirectX::XMFLOAT3& scale, const DirectX::XMFLOAT3& materialColor, const std::string& Path);

	DirectX::XMMATRIX GetTransformXM() const noexcept override;
	void Update(float dt) noexcept override;

private:
	DirectX::XMFLOAT3 position;
	DirectX::XMFLOAT3 velocity;
	DirectX::XMFLOAT3 rotation;
	DirectX::XMFLOAT3 angularvelocity;
	DirectX::XMFLOAT3 scale;

	DirectX::XMFLOAT3 materialColor;

	std::string modelPath;
};
#pragma once

#include "Drawable/DrawableBase.h"

#include "Graphics.h"
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <DirectXMath.h>

//A Drawable representing any 3D model
class Mesh : public DrawableBase<Mesh>
{
public:
	Mesh(Graphics& gfx, class std::vector<std::unique_ptr<class Bindable>> bindPtrs);
	void Draw( Graphics& gfx , DirectX::FXMMATRIX accumulatedTransform) const noexcept;
	void Update(float dt) noexcept override;
	DirectX::XMMATRIX GetTransformXM() const noexcept override;

private:
	mutable DirectX::XMFLOAT4X4 transform;
};

//Class to handle node tree transformations
class Node
{
friend class Model;

public:
	Node(const std::string& name, std::vector<Mesh*> meshPtrs, const DirectX::XMMATRIX& transform);
	
	inline void SetAppliedTransform(const DirectX::FXMMATRIX& matrix) noexcept { DirectX::XMStoreFloat4x4(&AppliedTransform, matrix); }
	inline void SetAppliedTransform(DirectX::XMFLOAT3 pos, DirectX::XMFLOAT3 rot, DirectX::XMFLOAT3 scale) noexcept
	{
		SetAppliedTransform(
			DirectX::XMMatrixScaling(scale.x, scale.y, scale.z) *
			DirectX::XMMatrixRotationRollPitchYaw(rot.x, rot.y, rot.z) *
			DirectX::XMMatrixTranslation(pos.x, pos.y, pos.z)
		);
	};

	void Draw( Graphics& gfx, DirectX::FXMMATRIX accumulatedTransform) const noexcept;
private:
	void AddChild(std::unique_ptr<Node> pChild) noexcept;

	std::string name;
	std::vector<std::unique_ptr<Node>> childPtrs;
	std::vector<Mesh*> meshPtrs;
	DirectX::XMFLOAT4X4 BaseTransform;
	mutable DirectX::XMFLOAT4X4 AppliedTransform;
};

//Helper class to load and parse 3D files and load them correctly as a scene tree
class Model
{
public:
	Model(Graphics& gfx, std::string filename);
	void Draw( Graphics& gfx, DirectX::FXMMATRIX transform ) const;

private:
	static std::unique_ptr<Mesh> ParseMesh(Graphics& gfx, const aiMesh& mesh);
	std::unique_ptr<Node> ParseNode(const aiNode& node);

	std::unique_ptr<Node> pRoot;
	std::vector<std::unique_ptr<Mesh>> meshPtrs;
};
#pragma once

#include "Drawable/DrawableBase.h"
#include "Vertex.h"

#include<DirectXMath.h>

class Box : public DrawableBase<Box>
{
public:
	Box(Graphics& gfx, DirectX::XMFLOAT4X4& transform, const DirectX::XMFLOAT3& materialColor);
	
	DirectX::XMMATRIX GetTransformXM() const noexcept override;
	void Update(float dt) noexcept override;

	~Box() override = default;
private:
	static VertexContainer vcont;

	alignas(16) DirectX::XMFLOAT3 materialColor;
	DirectX::XMFLOAT4X4& transform;
};
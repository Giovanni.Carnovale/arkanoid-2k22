#pragma once

#include "SystemHeaders.h"
#include <string>
#include <memory>

namespace Gdiplus{ class Color; }


namespace DirectX
{
	struct XMFLOAT4;
}

class Surface
{
public:
	Surface() = default;
	Surface(const std::wstring& filename);
	Surface(const Surface& other);
	Surface& operator=(const Surface& other);

	void PutPixel(int x, int y, Gdiplus::Color c) noexcept;
	Gdiplus::Color GetPixel(int x, int y) noexcept;
	inline int GetWidth()  const noexcept {return width;}
	inline int GetHeight()  const noexcept {return height;}
	inline Gdiplus::Color* GetBufferPtr() noexcept {return pPixels.get();}
	inline const Gdiplus::Color* GetBufferPtr() const noexcept {return pPixels.get();}

	static DirectX::XMFLOAT4 ColorAsVector(const Gdiplus::Color& c) noexcept;

private:
	int width;
	int height;
	std::shared_ptr<Gdiplus::Color[]> pPixels;
};
#pragma once

#include "GameObj/GameObj.h"
#include "Drawable/Box.h"

class Paddle final : public GameObj
{
public:
	Paddle(Graphics& gfx, const DirectX::XMFLOAT3& position, const DirectX::XMFLOAT3& rotation, const DirectX::XMFLOAT3& scale, const DirectX::XMFLOAT3& color);

	Paddle(const Paddle& other) = delete;
	Paddle& operator= (const Paddle& other) = delete;

	void Draw(class Graphics& gfx) const override;

	void Update(float dt) override;


	void OnCollision(const Hit&) override;
	
	inline void SetInput(const float InInput) const
	{
		InputAxis = InInput;
	}
private:
	void BoundsCheck();
	void SnapToClosestHorizontalEdge();

	Box PaddleMesh;
	const float Speed = 22.f;
	mutable float InputAxis = 0.f;
};
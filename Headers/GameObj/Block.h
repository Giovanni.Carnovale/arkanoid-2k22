#pragma once

#include "GameObj/GameObj.h"
#include "Drawable/Box.h"

class Block final : public GameObj
{
public:
	Block(Graphics& gfx, const DirectX::XMFLOAT3& position, const DirectX::XMFLOAT3& rotation, const DirectX::XMFLOAT3& scale, const DirectX::XMFLOAT3& color);

	Block(const Block& other) = delete;
	Block& operator= (const Block& other) = delete;

	void Draw(Graphics& gfx) const override;
	void Update(float dt) override;

#pragma region AABB

	inline float GetMinX() const {
		DirectX::XMVECTOR posVector, rotVector, scaleVector;
		DirectX::XMMatrixDecompose(&scaleVector, &rotVector, &posVector, GetTransformAsMatrix());
		DirectX::XMVECTOR minXVector = DirectX::XMVectorSubtract(posVector , DirectX::XMVectorScale(scaleVector, 0.5f));
		return DirectX::XMVectorGetX(minXVector);
	}

	inline float GetMaxX() const {
		DirectX::XMVECTOR posVector, rotVector, scaleVector;
		DirectX::XMMatrixDecompose(&scaleVector, &rotVector, &posVector, GetTransformAsMatrix());
		DirectX::XMVECTOR maxXVector = DirectX::XMVectorAdd(posVector, DirectX::XMVectorScale(scaleVector, 0.5f));
		return DirectX::XMVectorGetX(maxXVector);
	}

	inline float GetMinY() const {
		DirectX::XMVECTOR posVector, rotVector, scaleVector;
		DirectX::XMMatrixDecompose(&scaleVector, &rotVector, &posVector, GetTransformAsMatrix());
		DirectX::XMVECTOR minYVector = DirectX::XMVectorSubtract(posVector, DirectX::XMVectorScale(scaleVector, 0.5f));
		return DirectX::XMVectorGetY(minYVector);
	}

	inline float GetMaxY() const {
		DirectX::XMVECTOR posVector, rotVector, scaleVector;
		DirectX::XMMatrixDecompose(&scaleVector, &rotVector, &posVector, GetTransformAsMatrix());
		DirectX::XMVECTOR maxYVector = DirectX::XMVectorAdd(posVector, DirectX::XMVectorScale(scaleVector, 0.5f));
		return DirectX::XMVectorGetY(maxYVector);
	}
#pragma endregion

	~Block() override = default;
protected:
	inline void OnCollision(const Hit& hit) override {bReadyToDie = true;}
	
private:
	Box boxMesh;
};
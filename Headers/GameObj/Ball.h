#pragma once

#include "GameObj/GameObj.h"
#include "Drawable/Mesh.h"

class Ball final : public GameObj
{
public:
	Ball(Graphics& gfx, const DirectX::XMFLOAT3& position, const float radius, const float MaxSpeed);

	Ball(const Ball& other) = delete;
	Ball& operator= (const Ball& other) = delete;

	void Draw(Graphics& gfx) const override;
	void Update(float dt) override;

	void BoundsCheck();

	~Ball() override = default;

	inline float GetRadius() const noexcept {return Radius;}

protected:
	void OnCollision(const Hit&) override;

private:
	alignas(16) DirectX::XMFLOAT3 SpeedVector;
	alignas(16) Model SphereMesh;

	const float MaxSpeed;
	const float Radius;

	static constexpr const char* modelPath = "resources/models/sphere.obj";

};
#pragma once

#include <DirectXMath.h>
#include "Collision/Hit.h"
#include <optional>

class GameObj
{
friend class CollisionManager;

public:
	inline GameObj(const DirectX::XMFLOAT3& position, const DirectX::XMFLOAT3& rotation, const DirectX::XMFLOAT3& scale)
	: transform()
	{
		DirectX::XMStoreFloat4x4(
			&transform,
			DirectX::XMMatrixScaling(scale.x, scale.y, scale.z)*
			DirectX::XMMatrixRotationRollPitchYaw(rotation.x, rotation.y, rotation.z)*
			DirectX::XMMatrixTranslation(position.x, position.y, position.z)
		);
	};

	inline DirectX::XMMATRIX GetTransformAsMatrix() const { return DirectX::XMLoadFloat4x4(&transform); }

#pragma region SpacialGetters
	inline DirectX::XMFLOAT3 GetPosition() const noexcept
	{
		DirectX::XMVECTOR outScale, outRot, outPos;
		DirectX::XMMatrixDecompose(&outScale, &outRot, &outPos, DirectX::XMLoadFloat4x4(&transform));

		DirectX::XMFLOAT3 ReturnPos;
		DirectX::XMStoreFloat3(&ReturnPos, outPos);
		return ReturnPos;
	}

	inline float GetPositionX() const noexcept { return GetPosition().x; }
	inline float GetPositionY() const noexcept { return GetPosition().y; }
	inline float GetPositionZ() const noexcept { return GetPosition().z; }

	inline DirectX::XMFLOAT3 GetScale() const noexcept
	{
		DirectX::XMVECTOR outScale, outRot, outPos;
		DirectX::XMMatrixDecompose(&outScale, &outRot, &outPos, DirectX::XMLoadFloat4x4(&transform));

		DirectX::XMFLOAT3 ReturnScale;
		DirectX::XMStoreFloat3(&ReturnScale, outScale);
		return ReturnScale;
	}

	inline float GetScaleX() const noexcept { return GetScale().x; }
	inline float GetScaleY() const noexcept { return GetScale().y; }
	inline float GetScaleZ() const noexcept { return GetScale().z; }

	inline DirectX::XMFLOAT3 GetRotation() const noexcept
	{
		DirectX::XMVECTOR outScale, outRot, outPos;
		DirectX::XMMatrixDecompose(&outScale, &outRot, &outPos, DirectX::XMLoadFloat4x4(&transform));

		DirectX::XMFLOAT3 ReturnRotation;
		DirectX::XMStoreFloat3(&ReturnRotation, outRot);
		return ReturnRotation;
	}

	inline float GetRotationX() const noexcept { return GetRotation().x; }
	inline float GetRotationY() const noexcept { return GetRotation().y; }
	inline float GetRotationZ() const noexcept { return GetRotation().z; }
#pragma endregion SpacialGetters

	virtual void Draw(class Graphics& gfx) const = 0 {};
	virtual	void Update(float dt) = 0 {};
	
	inline bool IsReadyToDie() const { return bReadyToDie; };

 
	virtual ~GameObj() = 0 {};
protected:
	virtual void OnCollision(const Hit&) = 0 {};
	DirectX::XMFLOAT4X4 transform;
	bool bReadyToDie = false;
};
#pragma once

#include "Window.h"
#include "PointLight.h"
#include <vector>
#include <memory>
#include "GameObj/GameObj.h"
#include "Drawable/Drawable.h"
#include "Drawable/Mesh.h"

class App
{
public:
	App();

	int Go();
	void BuildBoard(int rows, int columns);

private:
	void DoFrame();

	float lastFrameTime = 0.01f;
	float timeScale = 1.0f;
		
	Window window;
	PointLight light;
	std::vector<std::unique_ptr<GameObj>> gameobjects;
	std::vector<std::unique_ptr<Drawable>> drawables;
	class Ball* pBallObj;
	class Paddle* pPlayerPaddle;
	bool GameOver;
};


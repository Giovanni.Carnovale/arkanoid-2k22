#pragma once

#include <DirectXMath.h>

struct Hit
{
	DirectX::XMFLOAT3 HitPos = {0.f, 0.f, 0.f};
	DirectX::XMFLOAT3 HitNormal = {0.f, 0.f, 0.f};

	class GameObj* pOther = nullptr;
};
#pragma once

#include <DirectXMath.h>


class CollisionManager
{
public:
	CollisionManager() = delete;
	CollisionManager(const CollisionManager& other) = delete;
	CollisionManager& operator=(const CollisionManager& other) = delete;

	static bool CheckCollision(class Ball& BallObj, class Block& BlockObj);
	static bool CheckCollision(class Ball& BallObj, class Paddle& PlayerPaddle);

private:
	static bool SphereToBoxCollision(
		const DirectX::XMFLOAT3 SpherePos, const float Radius, 
		const DirectX::XMFLOAT3 BoxPos, const DirectX::XMFLOAT3 BoxScale, DirectX::XMFLOAT3& OutHitPos, DirectX::XMFLOAT3& OutHitNormal);
};
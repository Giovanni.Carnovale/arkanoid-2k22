#pragma once

#include <vector>
#include <type_traits>

#include <DirectXMath.h>
#include "Graphics.h"
#include <cassert>


struct BGRAColor
{
	unsigned char a, r, g, b;
};



//Class defining some utilities methods and nested-classes to allow for simple vertex layout definition,
//interpretation, and checking of validity of informations.
class VertexLayout
{
public:
	enum class ElementType
	{
		Position2D = 1,
		Position3D = 1<<1,
		Texture2D = 1<<2,
		Normal = 1 << 3,
		Float3Color = 1 << 4,
		Float4Color = 1 << 5,
		BGRAColor = 1 << 6,
		Count = 1 << 7
	};
	
#pragma region MapDefinition

	template<ElementType> struct Map;
	template<> struct Map<ElementType::Position2D>
	{
		using SysType = DirectX::XMFLOAT2;
		static constexpr DXGI_FORMAT dxgiFormat = DXGI_FORMAT_R32G32_FLOAT;
		static constexpr const char* semantic = "Position";
	};

	template<> struct Map<ElementType::Position3D>
	{
		using SysType = DirectX::XMFLOAT3;
		static constexpr DXGI_FORMAT dxgiFormat = DXGI_FORMAT_R32G32B32_FLOAT;
		static constexpr const char* semantic = "Position";
	};

	template<> struct Map<ElementType::Texture2D>
	{
		using SysType = DirectX::XMFLOAT2;
		static constexpr DXGI_FORMAT dxgiFormat = DXGI_FORMAT_R32G32_FLOAT;
		static constexpr const char* semantic = "Texcoord";
	};

	template<> struct Map<ElementType::Normal>
	{
		using SysType = DirectX::XMFLOAT3;
		static constexpr DXGI_FORMAT dxgiFormat = DXGI_FORMAT_R32G32B32_FLOAT;
		static constexpr const char* semantic = "Normal";
	};

	template<> struct Map<ElementType::Float3Color>
	{
		using SysType = DirectX::XMFLOAT3;
		static constexpr DXGI_FORMAT dxgiFormat = DXGI_FORMAT_R32G32B32_FLOAT;
		static constexpr const char* semantic = "Color";
	};

	template<> struct Map<ElementType::Float4Color>
	{
		using SysType = DirectX::XMFLOAT4;
		static constexpr DXGI_FORMAT dxgiFormat = DXGI_FORMAT_R32G32B32A32_FLOAT;
		static constexpr const char* semantic = "Color";
	};

	template<> struct Map<ElementType::BGRAColor>
	{
		using SysType = DirectX::XMFLOAT2;
		static constexpr DXGI_FORMAT dxgiFormat = DXGI_FORMAT_R8G8B8A8_UNORM;
		static constexpr const char* semantic = "Color";
	};
#pragma endregion

	class Element
	{
	public:
		Element(ElementType type, size_t offset)
			: type(type), offset(offset) 
		{};

		inline size_t GetOffsetAfter() const noexcept { return offset + Size(); };
		inline size_t GetOffset() const noexcept { return offset; };
		inline size_t Size() const noexcept { return SizeOf(type); };

		static constexpr size_t SizeOf(ElementType type) noexcept
		{
			switch (type)
			{
			case ElementType::Position2D:
				return sizeof(Map<ElementType::Position2D>::SysType);
			case ElementType::Position3D:
				return sizeof(Map<ElementType::Position3D>::SysType);
			case ElementType::Texture2D:
				return sizeof(Map<ElementType::Texture2D>::SysType);
			case ElementType::Normal:
				return sizeof(Map<ElementType::Normal>::SysType);
			case ElementType::Float3Color:
				return sizeof(Map<ElementType::Float3Color>::SysType);
			case ElementType::Float4Color:
				return sizeof(Map<ElementType::Float4Color>::SysType);
			case ElementType::BGRAColor:
				return sizeof(Map<ElementType::BGRAColor>::SysType);
			default:
				assert("Invalid element type passed" && false);
				return 0u;
			}
		}

		inline ElementType GetType() const noexcept { return type; };
		D3D11_INPUT_ELEMENT_DESC GetDesc() const noexcept {
			switch (type)
			{
			case ElementType::Position2D:
				return GenerateDesc<ElementType::Position2D>(GetOffset());
			case ElementType::Position3D:
				return GenerateDesc<ElementType::Position3D>(GetOffset());
			case ElementType::Texture2D:
				return GenerateDesc<ElementType::Texture2D>(GetOffset());
			case ElementType::Normal:
				return GenerateDesc<ElementType::Normal>(GetOffset());
			case ElementType::Float3Color:
				return GenerateDesc<ElementType::Float3Color>(GetOffset());
			case ElementType::Float4Color:
				return GenerateDesc<ElementType::Float4Color>(GetOffset());
			case ElementType::BGRAColor:
				return GenerateDesc<ElementType::BGRAColor>(GetOffset());
			default:
				assert("Invalid element type" && false);
				return { "INVALID",0,DXGI_FORMAT_UNKNOWN,0,0,D3D11_INPUT_PER_VERTEX_DATA,0 };
			}
		};

	private:
		template<ElementType type>
		inline static constexpr D3D11_INPUT_ELEMENT_DESC GenerateDesc(size_t offset) noexcept
		{
			return { Map<type>::semantic, 0, Map<type>::dxgiFormat, 0, (UINT) offset, D3D11_INPUT_PER_VERTEX_DATA, 0 };
		}

		ElementType type;
		size_t offset;
	};

	template<ElementType type>
	const Element& Resolve() const noexcept
	{
		for (auto& e : elements)
		{
			if (e.GetType() == type)
			{
				return e;
			}
		}
		assert("Couldn't resolve type" && false);
		return elements.front();
	}

	inline const Element& ResolveByIndex(size_t i) const noexcept { return elements[i]; }
	inline VertexLayout& Append(ElementType type) { elements.emplace_back(type, Size()); return *this;}
	inline size_t Size() const noexcept { return elements.empty() ? 0u : elements.back().GetOffsetAfter(); }
	inline size_t GetElementCount() const noexcept { return elements.size(); }

	std::vector<D3D11_INPUT_ELEMENT_DESC> GetD3DLayout() const noexcept
	{
		std::vector<D3D11_INPUT_ELEMENT_DESC> desc;
		desc.reserve(GetElementCount());
		for(const auto& e : elements) 
		{
			desc.push_back(e.GetDesc());
		}
		return desc;
	};

private:
	std::vector<Element> elements;		
};

//Class that uses some template metaprogramming magic to allow for dynamic interpretation of data based on 
//the referenced vertex layout.
class Vertex
{
	friend class VertexContainer;
public:

	template<VertexLayout::ElementType Type>
	auto& Attribute() noexcept
	{
		auto pAttribute = pData + layout.Resolve<Type>().GetOffset();
		return *reinterpret_cast<typename VertexLayout::Map<Type>::SysType*>(pAttribute);
	}

	template<VertexLayout::ElementType Type>
	const auto& Attribute() const noexcept
	{
		auto pAttribute = pData + layout.Resolve<Type>().GetOffset();
		return *reinterpret_cast<typename VertexLayout::Map<Type>::SysType*>(pAttribute);
	}


	template<typename T>
	void SetAttributeByIndex(size_t i, T&& val) noexcept
	{
		const auto& element = layout.ResolveByIndex(i);
		auto pAttribute = pData + layout.ResolveByIndex(i).GetOffset();
		switch (element.GetType())
		{
		case VertexLayout::ElementType::Position2D:
			SetAttribute<VertexLayout::ElementType::Position2D>( pAttribute, std::forward<T>(val));
			break;
		case VertexLayout::ElementType::Position3D:
			SetAttribute<VertexLayout::ElementType::Position3D>(pAttribute, std::forward<T>(val));
			break;
		case VertexLayout::ElementType::Texture2D:
			SetAttribute<VertexLayout::ElementType::Texture2D>(pAttribute, std::forward<T>(val));
			break;
		case VertexLayout::ElementType::Normal:
			SetAttribute<VertexLayout::ElementType::Normal>(pAttribute, std::forward<T>(val));
			break;
		case VertexLayout::ElementType::Float3Color:
			SetAttribute<VertexLayout::ElementType::Float3Color>(pAttribute, std::forward<T>(val));
			break;
		case VertexLayout::ElementType::Float4Color:
			SetAttribute<VertexLayout::ElementType::Float4Color>(pAttribute, std::forward<T>(val));
			break;
		case VertexLayout::ElementType::BGRAColor:
			SetAttribute<VertexLayout::ElementType::BGRAColor>(pAttribute, std::forward<T>(val));
			break;
		default:
			assert("Bad element type" && false);
			break;

		}
	}

protected:
	Vertex(char* pData, const VertexLayout& layout) noexcept
		:
		pData(pData),
		layout(layout)
	{
		assert(pData != nullptr);
	}
private:
	template<typename First, typename ...Rest>
	void SetAttributeByIndex(size_t i, First&& first, Rest&&... rest) noexcept
	{
		SetAttributeByIndex(i, std::forward<First>(first));
		SetAttributeByIndex(i+1, std::forward<Rest>(rest)...);
	}

	template<VertexLayout::ElementType DestLayoutType, typename SrcType>
	void SetAttribute(char* pAttribute, SrcType&& val) noexcept
	{
		using Dest = typename VertexLayout::Map<DestLayoutType>::SysType;
		if constexpr (std::is_assignable<Dest, SrcType>::value)
		{
			*reinterpret_cast<Dest*>(pAttribute) = val;
		}
		else
		{
			assert("Parameter type mismatch" && false);
		}
	}



	char* pData = nullptr;
	const VertexLayout& layout;
};

class ConstVertex
{
public:
	ConstVertex(const Vertex& v) noexcept : vertex(v) {};

	template<VertexLayout::ElementType Type>
	const auto& Attribute() const noexcept
	{
		return vertex.Attribute<Type>();
	}
private:
	Vertex vertex;
};

class VertexContainer
{
public:
	VertexContainer( VertexLayout layout ) noexcept 
		: layout(std::move(layout)) 
	{}

	inline const char* GetData() const noexcept { return buffer.data(); }
	inline const VertexLayout& GetLayout() const noexcept { return layout; }
	inline size_t Count() const noexcept { return buffer.size()/layout.Size(); }
	inline size_t Size() const noexcept { return buffer.size(); }

	template<typename ...Params>
	void EmplaceBack(Params&&... params) noexcept
	{
		assert(sizeof...(params) == layout.GetElementCount() && "Param count not matching number of elements in layout");
		buffer.resize( buffer.size() + layout.Size() );
		Back().SetAttributeByIndex( 0u, std::forward<Params>(params)...);
	}

	inline Vertex Back() noexcept 
	{ 
		assert(buffer.size() != 0u); 
		return Vertex{ buffer.data() + buffer.size() - layout.Size(), layout }; 
	}

	inline Vertex Front() noexcept
	{
		assert(buffer.size() != 0u);
		return Vertex{ buffer.data(), layout };
	}

	Vertex operator[](size_t i) noexcept
	{
		return Vertex{ buffer.data() + layout.Size() * i, layout };
	}

	inline ConstVertex Back() const noexcept
	{
		return const_cast<VertexContainer*>(this)->Back();
	}

	inline ConstVertex Front() const noexcept
	{
		return const_cast<VertexContainer*>(this)->Front();
	}

	ConstVertex operator[](size_t i) const noexcept
	{
		return const_cast<VertexContainer&>(*this)[i];
	}

private:
	std::vector<char> buffer;
	VertexLayout layout;
};
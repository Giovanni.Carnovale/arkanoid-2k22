#pragma once

#include "Bindable/ConstantBuffer.h"
#include <DirectXMath.h>

class PointLight
{
public:
	PointLight(class Graphics& gfx, DirectX::XMFLOAT3 position = {0.0f, 0.0f, 0.0f}, float Intensity = 1.0f);
	void Draw(class Graphics& gfx) const noexcept;
	void Bind(class Graphics& gfx) const noexcept;

	inline void SetPosition(DirectX::XMFLOAT3 NewPosition) noexcept { WorldPos = NewPosition;}
	inline DirectX::XMFLOAT3 GetPosition() const noexcept {return WorldPos;}
	inline void SetAttenuation(float ConstAtt, float LinAtt, float QuadAtt) noexcept 
	{ 
		cb.AttConst = ConstAtt;
		cb.AttLin = LinAtt;
		cb.AttQuad = QuadAtt;
	}

	inline void SetIntensity(float intensity) noexcept { cb.Intensity = intensity; }
	inline float GetIntensity() const noexcept { return cb.Intensity; }

private:

	#pragma pack(4)	
	struct PointLightCBuff
	{
		alignas(16) float Intensity = 1.0f;	
		DirectX::XMFLOAT3 pos;

		alignas(16) float AttConst = 1.f;
		float AttLin = 0.14f;
		float AttQuad = 0.22f;
		
		alignas(16) DirectX::XMFLOAT3 AmbientLight = {0.1f, 0.1f, 0.1f};
	};

	mutable PointLightCBuff cb;
	mutable DirectX::XMFLOAT3 WorldPos;
	mutable PixelConstantBuffer<PointLightCBuff> cbuf;
};
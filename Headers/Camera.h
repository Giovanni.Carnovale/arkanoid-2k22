#pragma once

#include <DirectXMath.h>

class Camera
{
public:
	DirectX::XMMATRIX GetTransform() const noexcept;
	DirectX::XMMATRIX GetInverseTransform() const noexcept;
	inline DirectX::XMFLOAT3 GetPosition() const noexcept { return position; };
	inline DirectX::XMFLOAT3 GetRotation() const noexcept { return rotation; };
	inline void SetCamera(const DirectX::XMFLOAT3& pos, const DirectX::XMFLOAT3& rot) { position = pos; rotation = rot; }
	inline void Reset() noexcept { position = {0.0f, 0.0f, 0.0f}; rotation = {0.0f, 0.0f, 0.0f}; }

private:
	DirectX::XMFLOAT3 position;
	DirectX::XMFLOAT3 rotation;
};

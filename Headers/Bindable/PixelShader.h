#pragma once
#include "Bindable.h"

#include <d3d11.h>
#include <wrl.h>

class PixelShader : public Bindable
{
public:
	PixelShader(Graphics& gfx, const wchar_t* Path);
	void Bind(Graphics& gfx) noexcept override;
	inline ID3DBlob* GetBlob() noexcept { return pBlob.Get(); };

private:
	Microsoft::WRL::ComPtr<ID3D11PixelShader> pPixelShader;
	Microsoft::WRL::ComPtr<ID3DBlob> pBlob;
};
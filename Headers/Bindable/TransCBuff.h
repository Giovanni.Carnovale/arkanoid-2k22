#pragma once

#include <DirectXMath.h>
#include <memory>

#include "Bindable/Bindable.h"
#include "Bindable/ConstantBuffer.h"

class Drawable;

class TransCBuff : public Bindable
{
public:
	TransCBuff(Graphics& gfx, const Drawable& owner, UINT slot = 0u);
	void Bind(Graphics& gfx) noexcept override;

private:
	struct Transforms
	{
		DirectX::XMMATRIX model;
		DirectX::XMMATRIX MVP;
	};

	static std::unique_ptr<VertexConstantBuffer<Transforms>> pVcBuff;
	const Drawable& owner;
};
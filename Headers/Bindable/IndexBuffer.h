#pragma once
#include "Bindable.h"

#include <wrl.h>
#include <d3d11.h>
#include <vector>

class IndexBuffer : public Bindable
{
public:
	IndexBuffer(Graphics& gfx, const std::vector<unsigned short>& indices);
	void Bind(Graphics& gfx) noexcept override;
	inline UINT GetCount() const { return count; }
private:
	Microsoft::WRL::ComPtr<ID3D11Buffer> pIndexBuffer;
	UINT count;
};


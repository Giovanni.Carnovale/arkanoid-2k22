#pragma once

#include "Bindable/Bindable.h"
#include <wrl.h>

class Graphics;

class Sampler : public Bindable
{
public:
	Sampler(Graphics& gfx);
	void Bind(Graphics& gfx) noexcept override;
protected:
	Microsoft::WRL::ComPtr<struct ID3D11SamplerState> pSampler;
};


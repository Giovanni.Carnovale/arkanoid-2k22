#pragma once
#include "Bindable.h"

#include <d3dcommon.h>

class Topology : public Bindable
{
public:
	Topology(D3D_PRIMITIVE_TOPOLOGY type);
	void Bind(Graphics& gfx) noexcept override;
private:
	D3D_PRIMITIVE_TOPOLOGY type;
};


#pragma once

#include "Bindable/Bindable.h"
#include <wrl.h>

class Texture : public Bindable
{
public:
	Texture(Graphics& gfx, const class Surface& s);
	void Bind(Graphics& gfx) noexcept override;
protected:
	Microsoft::WRL::ComPtr<struct ID3D11ShaderResourceView> pTextureView;
};


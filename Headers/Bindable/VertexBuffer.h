#pragma once

#include "Bindable.h"
#include "GraphicsException.h"

#include <d3d11.h>
#include <wrl.h>


class VertexBuffer : public Bindable
{
public:
	VertexBuffer(Graphics& gfx, const class VertexContainer& vcont);
	void Bind(Graphics& gfx) noexcept override;

private:
	Microsoft::WRL::ComPtr<ID3D11Buffer> pVertexBuffer;
	const UINT stride; 
	const UINT offset = 0u;
};

#pragma once
#include "Bindable.h"

#include <d3d11.h>
#include <wrl.h>

#include "GraphicsException.h"


template<class BufferType>
class ConstantBuffer : public Bindable
{
public:
	ConstantBuffer(Graphics& gfx, UINT slot);
	ConstantBuffer(Graphics& gfx, UINT slot, const BufferType& cb);
	void Update(Graphics& gfx, const BufferType& cb);
	virtual void Bind(Graphics& gfx) noexcept = 0;

protected:
	UINT slot;
	Microsoft::WRL::ComPtr<ID3D11Buffer> pConstantBuffer;
};

template<class BT>
ConstantBuffer<BT>::ConstantBuffer(Graphics& gfx, UINT slot) : slot(slot)
{
	D3D11_BUFFER_DESC cbdesc = {};
	cbdesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cbdesc.Usage = D3D11_USAGE_DYNAMIC;
	cbdesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cbdesc.MiscFlags = 0u;
	cbdesc.ByteWidth = sizeof(BT);
	cbdesc.StructureByteStride = 0;

	HRESULT hr = GetDevice(gfx)->CreateBuffer(&cbdesc, nullptr, &pConstantBuffer);
	GFX_THROW_IF_FAILED(hr);
}

template<class BT>
ConstantBuffer<BT>::ConstantBuffer(Graphics& gfx, UINT slot, const BT& cb) : slot(slot)
{
	D3D11_BUFFER_DESC cbdesc = {};
	cbdesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cbdesc.Usage = D3D11_USAGE_DYNAMIC;
	cbdesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cbdesc.MiscFlags = 0u;
	cbdesc.ByteWidth = sizeof(cb);
	cbdesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA csubData = {};
	csubData.pSysMem = &cb;

	HRESULT hr = GetDevice(gfx)->CreateBuffer(&cbdesc, &csubData, &pConstantBuffer);
	GFX_THROW_IF_FAILED(hr);
}

template<class BT>
void ConstantBuffer<BT>::Update(Graphics& gfx, const BT& cb)
{
	D3D11_MAPPED_SUBRESOURCE msr;
	HRESULT hr = GetContext(gfx)->Map(
		pConstantBuffer.Get(), 0u, 
		D3D11_MAP_WRITE_DISCARD, 0u, &msr);
	GFX_THROW_IF_FAILED(hr);
	memcpy(msr.pData, &cb, sizeof(cb));
	GetContext(gfx)->Unmap(pConstantBuffer.Get(), 0u);
}

//##########################################
//#Constant Buffer Binding to Vertex Shader
//##########################################

template<class BufferType>
class VertexConstantBuffer : public ConstantBuffer<BufferType>
{
public:
	using ConstantBuffer<BufferType>::pConstantBuffer;
	using Bindable::GetContext;

	using ConstantBuffer<BufferType>::ConstantBuffer;
	virtual void Bind(Graphics& gfx) noexcept override;
};

template<class BT>
void VertexConstantBuffer<BT>::Bind(Graphics& gfx) noexcept {
	GetContext(gfx)->VSSetConstantBuffers(ConstantBuffer<BT>::slot, 1u, pConstantBuffer.GetAddressOf());
}

//##########################################
//#Constant Buffer Binding to Pixel Shader
//##########################################


template<class BufferType>
class PixelConstantBuffer : public ConstantBuffer<BufferType>
{
public:
	using ConstantBuffer<BufferType>::pConstantBuffer;
	using Bindable::GetContext;

	using ConstantBuffer<BufferType>::ConstantBuffer;
	virtual void Bind(Graphics& gfx) noexcept override;
};

template<class BT>
void PixelConstantBuffer<BT>::Bind(Graphics& gfx) noexcept {
	GetContext(gfx)->PSSetConstantBuffers(ConstantBuffer<BT>::slot, 1u, pConstantBuffer.GetAddressOf());
}

enum CBuffSlot
{
	LIGHT_C_BUFF = 0, OBJ_C_BUFF = 1 
};
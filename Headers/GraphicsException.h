#pragma once

#include <string>
#include "SystemHeaders.h"

class GraphicsException : public std::exception
{
public:
	GraphicsException(int line, const char* file, HRESULT hr) noexcept;
	char const* what() const override;
	static std::string TranslateErrorCode(HRESULT hr) noexcept;

private:
	HRESULT hr;
	int line;
	std::string file;
	mutable std::string whatBuffer;
};

#define GFX_THROW_IF_FAILED(hr) if( FAILED( hr ) ) throw GraphicsException(__LINE__, __FILE__, hr);
#define GFX_THROW_IF_REMOVED(hr) if (hr == DXGI_ERROR_DEVICE_REMOVED) { hr = pDevice->GetDeviceRemovedReason(); throw GraphicsException(__LINE__, __FILE__, hr); }



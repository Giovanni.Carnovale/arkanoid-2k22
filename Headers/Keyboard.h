#pragma once
#include <queue>
#include <bitset >

class Keyboard
{
	friend class Window;
public:
	class Event
	{
		public:
		enum class Type
		{ PRESS, RELEASE, INVALID };

		inline Event() noexcept : type(Type::INVALID), code(0u) {};
		inline Event(Type type, unsigned char code) noexcept : type(type), code(code) {};

		inline bool IsPress() const noexcept { return type == Type::PRESS; }
		inline bool IsRelease() const noexcept { return type == Type::RELEASE; }
		inline bool IsValid() const noexcept { return type != Type::INVALID; }

		inline unsigned char GetCode() const noexcept { return code; }

		private:
			Type type;
			unsigned char code;

	};

	Keyboard() = default;
	Keyboard( const Keyboard&) = delete;
	Keyboard& operator=( const Keyboard&) = delete;

	bool KeyIsPressed( unsigned char keycode ) const noexcept;
	Event ReadKey() noexcept;
	bool KeyIsEmpty() noexcept;
	void FlushKey() noexcept;

	char ReadChar() noexcept;
	bool CharIsEmpty() const noexcept;
	void FlushChar() noexcept;
	
	void Flush() noexcept;

	void EnableAutorepeat() noexcept;
	void DisableAutorepeat() noexcept;
	bool IsAutorepeatEnabled() noexcept;

private:
	//Window-side interface
	void OnKeyPressed( unsigned char keycode ) noexcept;
	void OnKeyReleased( unsigned char keycode ) noexcept;
	void OnChar( char character ) noexcept;
	void ClearState() noexcept;

	template<typename T>
	static void TrimBuffer( std::queue<T>& buffer) noexcept;

	static constexpr unsigned int nKeys = 256u;
	static constexpr unsigned int bufferSize = 16u;
	bool autorepeatEnabled = false;
	std::bitset<nKeys> keystates;
	std::queue<Event> keybuffer;
	std::queue<char> charbuffer;

};

template<typename T>
void Keyboard::TrimBuffer(std::queue<T>& buffer) noexcept
{
	while (buffer.size() > bufferSize)
	{
		buffer.pop();
	}
}


#pragma once

#include <vector>
#include <DirectXMath.h>

#include <cassert>

template<class T>
class IndexedTriangleList
{
public:
	std::vector<T> vertices;
	std::vector<unsigned short> indices;

	IndexedTriangleList() = default;
	IndexedTriangleList(std::vector<T> verts_in, std::vector<unsigned short> indices_in)
		: vertices(std::move(verts_in)), indices(std::move(indices_in))
	{
		assert(vertices.size()>2);
		assert(indices.size() % 3 == 0);
	}

	void Transform(DirectX::FXMMATRIX matrix)
	{
		for (auto& v : vertices)
		{
			const DirectX::XMVECTOR pos = DirectX::XMLoadFloat3(&v.pos);
			DirectX::XMStoreFloat3(
				&v.pos,
				DirectX::XMVector3Transform(pos, matrix)
			);
		}
	}

	void SetNormalsIndipendent() noexcept
	{
		namespace dx = DirectX;
		assert(indices.size() % 3 == 0 && indices.size() > 0);

		for (std::size_t i = 0; i < indices.size(); i+=3)
		{
			T& v0 = vertices[indices[i]];
			T& v1 = vertices[indices[i+1]];
			T& v2 = vertices[indices[i+2]];
			const auto p0 = XMLoadFloat3(&v0.pos);
			const auto p1 = XMLoadFloat3(&v1.pos);
			const auto p2 = XMLoadFloat3(&v2.pos);

			const auto n = dx::XMVector3Normalize(
				dx::XMVector3Cross(dx::XMVectorSubtract(p1, p0), dx::XMVectorSubtract(p2, p0))
			);

			dx::XMStoreFloat3(&v0.normal, n);
			dx::XMStoreFloat3(&v1.normal, n);
			dx::XMStoreFloat3(&v2.normal, n);
		}

	}
};
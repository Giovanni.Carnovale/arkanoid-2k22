#pragma once

#include "IndexedTriangleList.h"
#include <cassert>
#include <DirectXMath.h>
#include <vector>

#define _USE_MATH_DEFINES
#include <math.h>

class Prism
{
public:
	template<class V>
	static IndexedTriangleList<V> MakeTesselated(unsigned int baseSub = 16u)
	{
		namespace dx = DirectX;
		assert(baseSub >= 3u);

		const auto height = 2.0f;
		const auto base = dx::XMVectorSet(1.0f, 0.0f, -height / 2.0f, 0.0f);
		const auto offset = dx::XMVectorSet(0.0f, 0.0f, height, 0.0f);
		const float longituteAngle = static_cast<float>(2.0f * M_PI / baseSub);

		std::vector<V> vertices;

		vertices.emplace_back();
		vertices.back().pos = {0.0f, 0.0f, -height/2.0f};
		const unsigned short idxCenterBase = static_cast<unsigned short>(vertices.size() - 1);
		
		vertices.emplace_back();
		vertices.back().pos = { 0.0f, 0.0f, height/2.0f };
		const unsigned short idxCenterTop = static_cast<unsigned short>(vertices.size() - 1);

		for (unsigned int iLong = 0; iLong < baseSub; iLong++)
		{
			vertices.emplace_back();
			auto v = dx::XMVector3Transform(
				base,
				dx::XMMatrixRotationZ(longituteAngle * iLong)
			);
			dx::XMStoreFloat3(&vertices.back().pos, v);


			vertices.emplace_back();
			v = dx::XMVector3Transform(
				base,
				dx::XMMatrixRotationZ(longituteAngle * iLong)
			);
			v = dx::XMVectorAdd(v, offset);
			dx::XMStoreFloat3(&vertices.back().pos, v);
		}

		std::vector<unsigned short> indices;
		const int idxOffset = 2; //Insert indices from 2 to not insert cap vertices
		for (unsigned short iLong = 0; iLong < baseSub; iLong++)
		{
			const auto i = iLong * 2;
			const auto mod = baseSub * 2;

			indices.push_back(i + idxOffset);
			indices.push_back((i+ idxOffset) % mod + idxOffset);
			indices.push_back( i + idxOffset + 1);
			indices.push_back( (i + idxOffset ) % mod + idxOffset);
			indices.push_back( (i + idxOffset + 1 ) % mod + idxOffset);
			indices.push_back( i + idxOffset + 2);
		}

		for (unsigned short iLong = 0; iLong < baseSub; iLong++)
		{
			const auto i = iLong * 2;
			const auto mod = baseSub * 2;

			indices.push_back(i + 2);
			indices.push_back(idxCenterBase);
			indices.push_back((i + 2) % mod + 2);
			indices.push_back(idxCenterTop);
			indices.push_back(i + 1 + 2);
			indices.push_back((i + 3) % mod + 2);
		}

		return { std::move(vertices),std::move(indices) };
	}

	template<class V>
	static IndexedTriangleList<V> Make()
	{
		return MakeTesselated<V>(6u);
	}
};
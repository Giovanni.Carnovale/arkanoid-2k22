#pragma once

#include <vector>
#include <array>
#include <cassert>
#include <cmath>
#include "IndexedTriangleList.h"

class Plane
{
public:
	template<class V>
	static IndexedTriangleList<V> MakeTesselated(unsigned int division_x = 1u, unsigned int division_y = 1u)
	{
		namespace dx = DirectX;
		assert(division_x > 0);
		assert(division_y > 0);

		constexpr float width = 1.0f;
		constexpr float height = 1.0f;
		constexpr float halfSize_x = width/2;
		constexpr float halfSize_y = height/2;
		const int nVertices_x = division_x + 1;
		const int nVertices_y = division_y + 1;

		std::vector<V> vertices( nVertices_x * nVertices_y );

		const float divisionSize_x  = width/division_x;
		const float divisionSize_y  = height/division_y;
		const auto bottomleft = dx::XMVectorSet(-halfSize_x, -halfSize_y, 0.0f, 0.0f);
		int i = 0;
		for (int y = 0; y < nVertices_y; y++)
		{
			const float y_pos = float(y) * divisionSize_y;
			for (int x = 0; x < nVertices_x; x++)
			{
				const auto v =  dx::XMVectorAdd(
					bottomleft,
					dx::XMVectorSet(float(x) * divisionSize_x, y_pos, 0.0f, 0.0f)
				);
				dx::XMStoreFloat3(&vertices[i].pos, v);
				i++;
			}
		}

		std::vector<unsigned short> indices;
		const float indicesForSquare = std::powf(static_cast<float>(division_x * division_y), 2.0f);
		indices.reserve( static_cast<size_t>(indicesForSquare) * 6);
		
		const auto verticesToIndex = [nVertices_x](size_t x, size_t y)
		{
			//Index of the vertex in (x, y) is x + all the vertexes in the y row
			return (unsigned short) (y * nVertices_x + x);
		};

		for (size_t y = 0; y < division_y; y++)
		{
			for (size_t x = 0; x < division_x; x++)
			{
				const std::array<unsigned short, 4> indexArray =
				{
					verticesToIndex(x,y), verticesToIndex(x+1, y), verticesToIndex(x, y+1), verticesToIndex(x+1,y+1)
				};

				indices.push_back(indexArray[0]);
				indices.push_back(indexArray[2]);
				indices.push_back(indexArray[1]);
				indices.push_back(indexArray[1]);
				indices.push_back(indexArray[2]);
				indices.push_back(indexArray[3]);
			}
		}
		return {std::move(vertices), std::move(indices)};
	}
};
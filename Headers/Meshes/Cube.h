#pragma once

#include "Meshes/IndexedTriangleList.h"

class Cube
{
public:
	template<class V>
	static IndexedTriangleList<V> Make()
	{
		namespace dx = DirectX;

		constexpr float side = 1.0f;

		std::vector<dx::XMFLOAT3> vertices;
		vertices.emplace_back(-side, -side, -side);
		vertices.emplace_back(side, -side, -side);
		vertices.emplace_back(-side, side, -side);
		vertices.emplace_back(side, side, -side);		
		vertices.emplace_back(-side, -side, side);
		vertices.emplace_back(side, -side, side);
		vertices.emplace_back(-side, side, side);
		vertices.emplace_back(side, side, side);

		std::vector<V> verts(vertices.size());
		
		for (size_t i = 0; i < vertices.size(); i++)
		{
			verts[i].pos = vertices[i];
		}
		return {
			std::move(verts),
			{
			0,2,1, 2,3,1,
			1,3,5, 3,7,5,
			2,6,3, 3,6,7,
			4,5,7, 4,7,6,
			0,4,2, 2,4,6,
			0,1,4, 1,5,4
			}
		};
	}

	template<class V>
	static IndexedTriangleList<V> Make(const float length, const float height, const float width)
	{
		namespace dx = DirectX;

		const float hLength = 0.5f * length;
		const float hWidth = 0.5f * width;
		const float hHeight = 0.5f * height;

		std::vector<dx::XMFLOAT3> vertices;
		vertices.emplace_back(-hLength, -hHeight, -hWidth);
		vertices.emplace_back(hLength, -hHeight, -hWidth);
		vertices.emplace_back(-hLength, hHeight, -hWidth);
		vertices.emplace_back(hLength, hHeight, -hWidth);
		vertices.emplace_back(-hLength, -hHeight, hWidth);
		vertices.emplace_back(hLength, -hHeight, hWidth);
		vertices.emplace_back(-hLength, hHeight, hWidth);
		vertices.emplace_back(hLength, hHeight, hWidth);

		std::vector<V> verts(vertices.size());

		for (size_t i = 0; i < vertices.size(); i++)
		{
			verts[i].pos = vertices[i];
		}
		return {
			std::move(verts),
			{
			0,2,1, 2,3,1,
			1,3,5, 3,7,5,
			2,6,3, 3,6,7,
			4,5,7, 4,7,6,
			0,4,2, 2,4,6,
			0,1,4, 1,5,4
			}
		};
	}


	template<class V>
	static IndexedTriangleList<V> MakeIndipendent(const float length, const float height, const float width)
	{
		namespace dx = DirectX;

		std::vector<dx::XMFLOAT3> vertices(24);

		const float hLength = 0.5f * length;
		const float hWidth = 0.5f * width;
		const float hHeight = 0.5f * height;

		vertices[0] = { -hLength,-hHeight,-hWidth };// 0 near side
		vertices[1] = { hLength,-hHeight,-hWidth };// 1
		vertices[2] = { -hLength,hHeight,-hWidth };// 2
		vertices[3] = { hLength,hHeight,-hWidth };// 3
		vertices[4] = { -hLength,-hHeight,hWidth };// 4 far side
		vertices[5] = { hLength,-hHeight,hWidth };// 5
		vertices[6] = { -hLength,hHeight,hWidth };// 6
		vertices[7] = { hLength,hHeight,hWidth };// 7
		vertices[8] = { -hLength,-hHeight,-hWidth };// 8 left side
		vertices[9] = { -hLength,hHeight,-hWidth };// 9
		vertices[10] = { -hLength,-hHeight,hWidth };// 10
		vertices[11] = { -hLength,hHeight,hWidth };// 11
		vertices[12] = { hLength,-hHeight,-hWidth };// 12 right side
		vertices[13] = { hLength,hHeight,-hWidth };// 13
		vertices[14] = { hLength,-hHeight,hWidth };// 14
		vertices[15] = { hLength,hHeight,hWidth };// 15
		vertices[16] = { -hLength,-hHeight,-hWidth };// 16 bottom side
		vertices[17] = { hLength,-hHeight,-hWidth };// 17
		vertices[18] = { -hLength,-hHeight,hWidth };// 18
		vertices[19] = { hLength,-hHeight,hWidth };// 19
		vertices[20] = { -hLength,hHeight,-hWidth };// 20 top side
		vertices[21] = { hLength,hHeight,-hWidth };// 21
		vertices[22] = { -hLength,hHeight,hWidth };// 22
		vertices[23] = { hLength,hHeight,hWidth };// 23

		std::vector<V> verts(vertices.size());

		for (size_t i = 0; i < vertices.size(); i++)
		{
			verts[i].pos = vertices[i];
		}
		return {
			std::move(verts),
			{
				0,2, 1,    2,3,1,
				4,5, 7,    4,7,6,
				8,10, 9,  10,11,9,
				12,13,15, 12,15,14,
				16,17,18, 18,17,19,
				20,23,21, 20,22,23
			}
		};
	}

	template<class V>
	static IndexedTriangleList<V> MakeIndipendent()
	{
		namespace dx = DirectX;

		constexpr float side = 0.5f;

		std::vector<dx::XMFLOAT3> vertices(24);

		vertices[0] = { -side,-side,-side };// 0 near side
		vertices[1] = { side,-side,-side };// 1
		vertices[2] = { -side,side,-side };// 2
		vertices[3] = { side,side,-side };// 3
		vertices[4] = { -side,-side,side };// 4 far side
		vertices[5] = { side,-side,side };// 5
		vertices[6] = { -side,side,side };// 6
		vertices[7] = { side,side,side };// 7
		vertices[8] = { -side,-side,-side };// 8 left side
		vertices[9] = { -side,side,-side };// 9
		vertices[10] = { -side,-side,side };// 10
		vertices[11] = { -side,side,side };// 11
		vertices[12] = { side,-side,-side };// 12 right side
		vertices[13] = { side,side,-side };// 13
		vertices[14] = { side,-side,side };// 14
		vertices[15] = { side,side,side };// 15
		vertices[16] = { -side,-side,-side };// 16 bottom side
		vertices[17] = { side,-side,-side };// 17
		vertices[18] = { -side,-side,side };// 18
		vertices[19] = { side,-side,side };// 19
		vertices[20] = { -side,side,-side };// 20 top side
		vertices[21] = { side,side,-side };// 21
		vertices[22] = { -side,side,side };// 22
		vertices[23] = { side,side,side };// 23

		std::vector<V> verts(vertices.size());

		for (size_t i = 0; i < vertices.size(); i++)
		{
			verts[i].pos = vertices[i];
		}
		return {
			std::move(verts),
			{
				0,2, 1,    2,3,1,
				4,5, 7,    4,7,6,
				8,10, 9,  10,11,9,
				12,13,15, 12,15,14,
				16,17,18, 18,17,19,
				20,23,21, 20,22,23
			}
		};
	}
};
#pragma once

#include "GDIPlusManager.h"

#include "SystemHeaders.h"

#include <algorithm>
namespace Gdiplus{ using std::min; using std::max; };
#include <gdiplus.h>

ULONG_PTR GDIPlusManager::token = 0;
int GDIPlusManager::refCount = 0;

GDIPlusManager::GDIPlusManager()
{
	if (refCount == 0)
	{
		Gdiplus::GdiplusStartupInput input{nullptr, FALSE, FALSE};
		input.GdiplusVersion = 1;
		Gdiplus::GdiplusStartup(&token, &input, nullptr);
	}
	refCount++;
}

GDIPlusManager::~GDIPlusManager()
{
	refCount--;
	if (refCount == 0)
	{
		Gdiplus::GdiplusShutdown(token);
	}
}

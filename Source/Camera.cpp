#include "Camera.h"

DirectX::XMMATRIX Camera::GetTransform() const noexcept
{
	return (
		DirectX::XMMatrixRotationRollPitchYaw(rotation.x, rotation.y, rotation.z) *
		DirectX::XMMatrixTranslation(position.x, position.y, position.z)
	);
}

DirectX::XMMATRIX Camera::GetInverseTransform() const noexcept
{	
	DirectX::XMMATRIX Transform = GetTransform();
	DirectX::XMVECTOR determinant = DirectX::XMMatrixDeterminant(Transform);
	return DirectX::XMMatrixInverse(
			&determinant , Transform
		);
}

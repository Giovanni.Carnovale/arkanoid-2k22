#include "Surface.h"

#include "SystemHeaders.h"
#include "GDIHeaders.h"

namespace gdi = Gdiplus;

#include <cassert>
#include <fstream>
#include <string>


#include <DirectXMath.h>
#include <limits>

Surface::Surface(const std::wstring& filename)
{
	gdi::Bitmap bitmap(filename.c_str());

	if (bitmap.GetLastStatus() != gdi::Ok)
	{
		std::string narrowFilename;
		std::transform(
			filename.begin(), filename.end(), 
			std::back_inserter(narrowFilename), 
			[](wchar_t c){return static_cast<char>(c);}
		);
		throw std::runtime_error("Surface failed to load file: " + narrowFilename);
	}

	width = bitmap.GetWidth();
	height = bitmap.GetHeight();
	pPixels = std::shared_ptr<gdi::Color[]>(new gdi::Color[width*height]);

	const BOOL isAlpha = gdi::IsAlphaPixelFormat(bitmap.GetPixelFormat());

	for (int y = 0; y < height; y++)
	{
		for (int x = 0; x < width; x++)
		{
			gdi::Color pixel;
			bitmap.GetPixel(x, y, &pixel);

			if (isAlpha == TRUE)
			{
				PutPixel(x, y, {pixel.GetA(), pixel.GetB(), pixel.GetG(), pixel.GetR()});
			}
			else
			{
				PutPixel(x, y, {  pixel.GetB(), pixel.GetG(), pixel.GetR() });
			}
		}
	}
}


Surface::Surface(const Surface& other)
	: width(other.width), height(other.height), pPixels(other.pPixels)
{}

Surface& Surface::operator=(const Surface& other)
{

	if (this != &other)
	{
		width = other.width;
		height = other.height;
		pPixels = other.pPixels;
	}
	return *this;
}

void Surface::PutPixel(int x, int y, gdi::Color c) noexcept
{
	assert(x >= 0);
	assert(x < width);
	assert(y >= 0);
	assert(y < height);
	pPixels[y * width + x] = c;
}

Gdiplus::Color Surface::GetPixel(int x, int y) noexcept
{
	return pPixels[y*width + x];
}

DirectX::XMFLOAT4 Surface::ColorAsVector(const Gdiplus::Color& c) noexcept
{
	float alpha = static_cast<float>(c.GetA() / std::numeric_limits<BYTE>::max());
	float red = static_cast<float>(c.GetR() / std::numeric_limits<BYTE>::max());
	float green = static_cast<float>(c.GetG() / std::numeric_limits<BYTE>::max());
	float blue = static_cast<float>(c.GetB() / std::numeric_limits<BYTE>::max());

	return DirectX::XMFLOAT4{alpha, red, green, blue};
}


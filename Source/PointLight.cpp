#include "PointLight.h"
#include "Graphics.h"

PointLight::PointLight(Graphics& gfx, DirectX::XMFLOAT3 position, float Intensity) : WorldPos(position), cbuf(gfx, CBuffSlot::LIGHT_C_BUFF), cb()
{
	cb.Intensity = Intensity;
}

void PointLight::Draw(Graphics& gfx) const noexcept
{

}

void PointLight::Bind(Graphics& gfx) const noexcept
{
	auto PositionVector = DirectX::XMLoadFloat3(&WorldPos);
	DirectX::XMStoreFloat3(&cb.pos, DirectX::XMVector3Transform(PositionVector, gfx.GetViewTransform()));
	cbuf.Update(gfx, cb);
	cbuf.Bind(gfx);
}

#include "Bindable/VertexShader.h"

#include <d3dcompiler.h>

#include "GraphicsException.h"

VertexShader::VertexShader(Graphics& gfx, const wchar_t* Path)
{
	
	HRESULT hr = D3DReadFileToBlob(Path, &pBlob);
	GFX_THROW_IF_FAILED(hr);

	hr = GetDevice(gfx)->CreateVertexShader(pBlob->GetBufferPointer(), pBlob->GetBufferSize(), nullptr, &pVertexShader);
	GFX_THROW_IF_FAILED(hr);
}

void VertexShader::Bind(Graphics& gfx) noexcept
{
	GetContext(gfx)->VSSetShader(pVertexShader.Get(), nullptr, 0);
}

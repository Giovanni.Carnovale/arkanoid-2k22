#include "Bindable/IndexBuffer.h"
#include "GraphicsException.h"

IndexBuffer::IndexBuffer(Graphics& gfx, const std::vector<unsigned short>& indices)
: count(UINT(indices.size()))
{
	D3D11_BUFFER_DESC ibd = {};
	ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	ibd.Usage = D3D11_USAGE_DEFAULT;
	ibd.CPUAccessFlags = 0u;
	ibd.MiscFlags = 0u;
	ibd.ByteWidth = UINT(sizeof(unsigned short) * indices.size());
	ibd.StructureByteStride = sizeof(unsigned short);
	D3D11_SUBRESOURCE_DATA idxData = {};
	idxData.pSysMem = indices.data();

	HRESULT hr = GetDevice(gfx)->CreateBuffer(&ibd, &idxData, &pIndexBuffer);
	GFX_THROW_IF_FAILED(hr);
}

void IndexBuffer::Bind(Graphics& gfx) noexcept
{
	GetContext(gfx)->IASetIndexBuffer(pIndexBuffer.Get(), DXGI_FORMAT_R16_UINT, 0);
}

#include "Bindable/InputLayout.h"

#include "GraphicsException.h"

InputLayout::InputLayout(Graphics& gfx, const std::vector<D3D11_INPUT_ELEMENT_DESC>& layout, ID3DBlob* pVertexShaderBytecode)
{
	HRESULT hr = GetDevice(gfx)->CreateInputLayout(layout.data(), (UINT)std::size(layout), pVertexShaderBytecode->GetBufferPointer(), pVertexShaderBytecode->GetBufferSize(), &pInputLayout);
	GFX_THROW_IF_FAILED(hr);
}

void InputLayout::Bind(Graphics& gfx) noexcept
{
	GetContext(gfx)->IASetInputLayout(pInputLayout.Get());
}

#include "Bindable/PixelShader.h"

#include <d3dcompiler.h>

#include "GraphicsException.h"


PixelShader::PixelShader(Graphics& gfx, const wchar_t* Path)
{

	HRESULT hr = D3DReadFileToBlob(Path, &pBlob);
	GFX_THROW_IF_FAILED(hr);

	hr = GetDevice(gfx)->CreatePixelShader(pBlob->GetBufferPointer(), pBlob->GetBufferSize(), nullptr, &pPixelShader);
	GFX_THROW_IF_FAILED(hr);
}

void PixelShader::Bind(Graphics& gfx) noexcept
{
	GetContext(gfx)->PSSetShader(pPixelShader.Get(), nullptr, 0);
}

#include "Bindable/Topology.h"

#include <d3d11.h>

Topology::Topology(D3D_PRIMITIVE_TOPOLOGY type)
: type(type)
{

}

void Topology::Bind(Graphics& gfx) noexcept
{
	GetContext(gfx)->IASetPrimitiveTopology(type);
}

#include "Bindable/VertexBuffer.h"

#include "Graphics.h"
#include "GraphicsException.h"
#include <wrl.h>
#include "Vertex.h"
#include <d3d11.h>

VertexBuffer::VertexBuffer(Graphics& gfx, const VertexContainer& vcont)
: stride(UINT(vcont.GetLayout().Size()))
{
	D3D11_BUFFER_DESC desc = {};
	desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	desc.Usage = D3D11_USAGE_DEFAULT;
	desc.CPUAccessFlags = 0u;
	desc.MiscFlags = 0u;
	desc.ByteWidth = UINT(vcont.Size());
	desc.StructureByteStride = stride;

	D3D11_SUBRESOURCE_DATA data = {};

	data.pSysMem = vcont.GetData();

	HRESULT hr = GetDevice(gfx)->CreateBuffer(&desc, &data, &pVertexBuffer);
	GFX_THROW_IF_FAILED(hr);
}

void VertexBuffer::Bind(Graphics& gfx) noexcept
{
	GetContext(gfx)->IASetVertexBuffers(0u, 1u, pVertexBuffer.GetAddressOf(), &stride, &offset);
}


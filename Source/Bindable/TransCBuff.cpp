#include "Bindable/TransCBuff.h"

#include "Bindable/VertexBuffer.h"
#include "Drawable/Drawable.h"



TransCBuff::TransCBuff(Graphics& gfx, const Drawable& owner, UINT slot /*= 0u*/)
: owner(owner)
{
	if (!pVcBuff)
	{
		pVcBuff = std::make_unique<VertexConstantBuffer<TransCBuff::Transforms>>(gfx, slot);
	}
}

void TransCBuff::Bind(Graphics& gfx) noexcept
{
	const auto ModelView = DirectX::XMMatrixTranspose(owner.GetTransformXM() * gfx.GetViewTransform());
	const auto MVP =  DirectX::XMMatrixTranspose(
		gfx.GetProjection()
	) * ModelView; 

	pVcBuff->Update(gfx, { ModelView, MVP});
	pVcBuff->Bind(gfx);
}

std::unique_ptr<VertexConstantBuffer<TransCBuff::Transforms>> TransCBuff::pVcBuff{};
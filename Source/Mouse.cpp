#include "Mouse.h"
#include "SystemHeaders.h"

std::pair<int, int> Mouse::GetPos() const noexcept
{
	return {x, y};
}

Mouse::Event Mouse::Read() noexcept
{
	if (buffer.size() > 0)
	{
		Mouse::Event e = buffer.front();
		buffer.pop();
		return e;
	}
	else
	{
		return Mouse::Event();
	}
}

void Mouse::Flush() noexcept
{
	buffer = std::queue<Event>();
}

void Mouse::OnMouseMove(int x, int y) noexcept
{
	this->x = x;;
	this->y = y;;

	buffer.push( Mouse::Event(Mouse::Event::Type::MOVE, *this));
	TrimBuffer();
}

void Mouse::OnLeftPressed(int x, int y) noexcept
{
	this->x = x;;
	this->y = y;;
	isLeftPressed = true;

	buffer.push(Mouse::Event(Mouse::Event::Type::LPRESS, *this));
	TrimBuffer();
}

void Mouse::OnLeftReleased(int x, int y) noexcept
{
	this->x = x;;
	this->y = y;;
	isLeftPressed = false;

	buffer.push(Mouse::Event(Mouse::Event::Type::LRELEASE, *this));
	TrimBuffer();
}

void Mouse::OnRightPressed(int x, int y) noexcept
{
	this->x = x;;
	this->y = y;;
	isRightPressed = true;

	buffer.push(Mouse::Event(Mouse::Event::Type::RPRESS, *this));
	TrimBuffer();
}

void Mouse::OnRightReleased(int x, int y) noexcept
{
	this->x = x;;
	this->y = y;;
	isRightPressed = false;

	buffer.push(Mouse::Event(Mouse::Event::Type::RRELEASE, *this));
	TrimBuffer();
}

void Mouse::OnWheelUp(int x, int y) noexcept
{
	this->x = x;;
	this->y = y;;

	buffer.push(Mouse::Event(Mouse::Event::Type::WHEELUP, *this));
	TrimBuffer();
}

void Mouse::OnWheelDown(int x, int y) noexcept
{
	this->x = x;;
	this->y = y;;

	buffer.push(Mouse::Event(Mouse::Event::Type::WHEELDOWN, *this));
	TrimBuffer();
}

void Mouse::OnMouseEnter() noexcept
{
	isInWindow = true;

	buffer.push(Mouse::Event(Mouse::Event::Type::ENTER, *this));
	TrimBuffer();
}

void Mouse::OnMouseLeave() noexcept
{
	isInWindow = false;

	buffer.push(Mouse::Event(Mouse::Event::Type::LEAVE, *this));
	TrimBuffer();
}

void Mouse::OnWheelDelta( int x, int y, int delta ) noexcept
{
	wheelDeltaCarry += delta;

	while (wheelDeltaCarry >= WHEEL_DELTA)
	{
		wheelDeltaCarry -= WHEEL_DELTA;
		OnWheelUp(x,y);
	}
	while (wheelDeltaCarry <= -WHEEL_DELTA)
	{
		wheelDeltaCarry += WHEEL_DELTA;
		OnWheelDown(x, y);
	}
}

void Mouse::TrimBuffer() noexcept
{
	while (buffer.size() > bufferSize)
	{
		buffer.pop();
	}
}

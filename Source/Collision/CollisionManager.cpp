#include "Collision/CollisionManager.h"

#include <cassert>
#include "GameObj/Ball.h"
#include "GameObj/Block.h"
#include "GameObj/Paddle.h"

namespace dx = DirectX;

bool CollisionManager::CheckCollision(Ball& BallObj, Block& BlockObj)
{
	Hit h;
	
	if (SphereToBoxCollision(BallObj.GetPosition(), BallObj.GetRadius(), BlockObj.GetPosition(), BlockObj.GetScale(), h.HitPos, h.HitNormal))
	{
		h.pOther = &BlockObj;

		Hit h2 = h;
		h2.pOther = &BallObj;

		static_cast<GameObj&>(BallObj).OnCollision(h);
		static_cast<GameObj&>(BlockObj).OnCollision(h2);
		return true;
	}
	else 
	{
		return false;
	}
}

bool CollisionManager::CheckCollision(Ball& BallObj, Paddle& PlayerPaddle)
{
	Hit h;

	if (SphereToBoxCollision(BallObj.GetPosition(), BallObj.GetRadius(), PlayerPaddle.GetPosition(), PlayerPaddle.GetScale(), h.HitPos, h.HitNormal))
	{
		h.pOther = &PlayerPaddle;

		Hit h2 = h;
		h2.pOther = &BallObj;

		static_cast<GameObj&>(BallObj).OnCollision(h);
		static_cast<GameObj&>(PlayerPaddle).OnCollision(h2);
		return true;
	}
	else
	{
		return false;
	}
	return false;
}

bool CollisionManager::SphereToBoxCollision(const DirectX::XMFLOAT3 SpherePos, const float Radius, const DirectX::XMFLOAT3 BoxPos, const DirectX::XMFLOAT3 BoxScale, DirectX::XMFLOAT3& OutHitPos, DirectX::XMFLOAT3& OutHitNormal)
{
	//Get Closest Point To Sphere
	const float x = std::max<float>(BoxPos.x - 0.5f * BoxScale.x, std::min<float>(BoxPos.x + 0.5f * BoxScale.x, SpherePos.x));
	const float y = std::max<float>(BoxPos.y - 0.5f * BoxScale.y, std::min<float>(BoxPos.y + 0.5f * BoxScale.y, SpherePos.y));

	const float distanceSqrd =
		(x - SpherePos.x) * (x - SpherePos.x) +
		(y - SpherePos.y) * (y - SpherePos.y);

	if (distanceSqrd < Radius)
	{
		OutHitPos = dx::XMFLOAT3{ x, y, 0.0f };
		OutHitNormal = dx::XMFLOAT3{ SpherePos.x - x, SpherePos.y - y, 0.0f };
		dx::XMStoreFloat3(
			&OutHitNormal,
			dx::XMVector3Normalize(dx::XMLoadFloat3(&OutHitNormal))
		);
		return true;
	}
	else
	{
		return false;
	}
}

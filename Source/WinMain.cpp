#include "App.h"
#include "GDIPlusManager.h"

int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow);

int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	GDIPlusManager gdipManager{};

	try
	{		
		App app;
		return app.Go();
	}
	catch (const WindowException& e)
	{
		MessageBox( nullptr, e.what(), "Window Exception", MB_OK | MB_ICONEXCLAMATION );
	} 
	catch (const std::exception& e)
	{
		MessageBox(nullptr, e.what(), "Standard Exception", MB_OK | MB_ICONEXCLAMATION);
	}
	catch (...)
	{
		MessageBox(nullptr, "No Details Available", "Undefined Exception", MB_OK | MB_ICONEXCLAMATION);
	}

	return -1;
}

#include "Graphics.h"

#include <vector>
#include <sstream>
#include <d3d11.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>
#include "GraphicsException.h"


//Bindables
#include "Bindable/VertexBuffer.h"
#include "Bindable/IndexBuffer.h"
#include "Bindable/ConstantBuffer.h"
#include "Bindable/VertexShader.h"
#include "Bindable/PixelShader.h"
#include "Bindable/InputLayout.h"
#include "Bindable/Topology.h"

namespace wrl = Microsoft::WRL;
namespace  dx = DirectX;


Graphics::Graphics(HWND hWnd)
{
	HRESULT result;


	DXGI_SWAP_CHAIN_DESC sd = {};
	sd.BufferDesc.Width = 0;
	sd.BufferDesc.Height = 0;
	sd.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	sd.BufferDesc.RefreshRate.Numerator = 0;
	sd.BufferDesc.RefreshRate.Denominator = 0;
	sd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	sd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	sd.SampleDesc.Count = 1;
	sd.SampleDesc.Quality = 0;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.BufferCount = 1;
	sd.OutputWindow = hWnd;
	sd.Windowed = TRUE;
	sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	sd.Flags = 0;
	
	UINT Flags = 0;
	#if _DEBUG
	Flags |= D3D11_CREATE_DEVICE_DEBUG;
	#endif

	result = D3D11CreateDeviceAndSwapChain(
		nullptr,
		D3D_DRIVER_TYPE_HARDWARE,
		nullptr,
		Flags,
		nullptr,
		0,
		D3D11_SDK_VERSION,
		&sd,
		&pSwap,
		&pDevice,
		nullptr,
		&pContext
	);
	GFX_THROW_IF_FAILED(result);

	wrl::ComPtr<ID3D11Resource> pBackBuffer = nullptr;
	result = pSwap->GetBuffer(0, __uuidof(ID3D11Resource), &pBackBuffer);
	GFX_THROW_IF_FAILED(result);

	result = pDevice->CreateRenderTargetView(
		pBackBuffer.Get(),
		nullptr,
		&pTarget
	);
	GFX_THROW_IF_FAILED(result);


	D3D11_DEPTH_STENCIL_DESC dsDesc = {};
	dsDesc.DepthEnable = TRUE;
	dsDesc.DepthFunc = D3D11_COMPARISON_LESS;
	dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;

	wrl::ComPtr<ID3D11DepthStencilState> pDSSState;

	result = pDevice->CreateDepthStencilState(&dsDesc, &pDSSState);
	GFX_THROW_IF_FAILED(result);
	pContext->OMSetDepthStencilState(pDSSState.Get(), 1u);

	wrl::ComPtr<ID3D11Texture2D> pDepthStencil;
	D3D11_TEXTURE2D_DESC descDepth = {};
	descDepth.Width = 1280u;
	descDepth.Height = 720u;
	descDepth.MipLevels = 1u;
	descDepth.ArraySize = 1u;
	descDepth.Format = DXGI_FORMAT_D32_FLOAT;
	descDepth.SampleDesc = {1u, 0u};
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;

	result = pDevice->CreateTexture2D(&descDepth, nullptr, &pDepthStencil);
	GFX_THROW_IF_FAILED(result);

	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV = {};
	descDSV.Format = DXGI_FORMAT_D32_FLOAT;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;
	result = pDevice->CreateDepthStencilView(pDepthStencil.Get(), &descDSV, &pDepthStencilView);
	GFX_THROW_IF_FAILED(result);

	pContext->OMSetRenderTargets(1u, pTarget.GetAddressOf(), pDepthStencilView.Get());

	D3D11_VIEWPORT vp = {};
	vp.Width = 1280;
	vp.Height = 720;
	vp.MinDepth = 0;
	vp.MaxDepth = 1;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	pContext->RSSetViewports(1u, &vp);

	camera.SetCamera({0.0f, 0.0f, -10.0f}, {0.0f, 0.0f, 0.0f});
}

void Graphics::ClearBuffer(float red, float green, float blue) noexcept
{
	const float color[] = {red, green, blue, 1.0f};
	pContext->ClearRenderTargetView(pTarget.Get(), color);
	pContext->ClearDepthStencilView(pDepthStencilView.Get(), D3D11_CLEAR_DEPTH, 1.0f, 0);
}

void Graphics::DrawIndexed(UINT count) const noexcept
{

	pContext->DrawIndexed(count, 0u, 0u);
}

void Graphics::EndFrame()
{
	HRESULT hr;
	hr = pSwap->Present( 1u, 0u );
	GFX_THROW_IF_FAILED(hr);
	GFX_THROW_IF_REMOVED(hr);
}


//#################################
//Graphic Exceptions
//#################################

GraphicsException::GraphicsException(int line, const char* file, HRESULT hr) noexcept
	: line(line), file(file), hr(hr) {}

char const* GraphicsException::what() const
{
	std::ostringstream oss;
	oss << "Graphics Exception" << std::endl
		<< "Error Code: " << hr << std::endl
		<< "Error String: " << TranslateErrorCode(hr) << std::endl
		<< "[FILE] " << file << std::endl
		<< "[LINE] " << line;
	whatBuffer = oss.str();
	return whatBuffer.c_str();
}

std::string GraphicsException::TranslateErrorCode(HRESULT hr) noexcept
{
	char* pMsgBuff = nullptr;
	DWORD nMsgLen = FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		nullptr, hr, MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US),
		reinterpret_cast<LPSTR>(&pMsgBuff), 0, nullptr
	);

	if (nMsgLen == 0)
	{
		return "Unidentified error code";
	}

	std::string errorString = pMsgBuff;
	LocalFree(pMsgBuff);
	return errorString;
}

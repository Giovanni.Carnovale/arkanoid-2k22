#include "Window.h"
#include <assert.h>
#include <sstream>


//####################
//WindowClass
//####################

Window::WindowClass Window::WindowClass::Instance;

const char* Window::WindowClass::GetName() noexcept
{
	return wndClassName;
}

HINSTANCE Window::WindowClass::GetInstance() noexcept
{
	return Instance.hInst;
}

Window::WindowClass::WindowClass() noexcept
: hInst( GetModuleHandle(nullptr) )
{
	WNDCLASSEX wc = { 0 };
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.lpfnWndProc = HandleMsgSetup;
	wc.hInstance = hInst;
	wc.lpszClassName = wndClassName;
	wc.style = CS_OWNDC;
	wc.hIcon = static_cast<HICON>(LoadImage(hInst, MAKEINTRESOURCE( IDI_ICON1 ), IMAGE_ICON, 32, 32, 0));
	wc.hIconSm = static_cast<HICON>(LoadImage(hInst, MAKEINTRESOURCE( IDI_ICON1 ), IMAGE_ICON, 16, 16, 0));
	RegisterClassEx(&wc);
}

Window::WindowClass::~WindowClass()
{
	UnregisterClass(wndClassName, GetInstance());
}



//####################
//Window
//####################

Window::Window(int width, int height, const char* name)
: width(width), height(height)
{
	RECT wr = {0};
	wr.right = width;
	wr.bottom = height;

	constexpr DWORD style = WS_CAPTION | WS_MINIMIZEBOX | WS_SYSMENU;

	BOOL gResult = AdjustWindowRect(&wr, style, FALSE);
	if (gResult == 0)
	{
		throw WND_LASTEXCEPT();
	}

	hWnd = CreateWindowEx(
		0,
		WindowClass::GetName(), name,
		style,
		CW_USEDEFAULT, CW_USEDEFAULT, wr.right - wr.left, wr.bottom - wr.top,
		nullptr, nullptr, WindowClass::GetInstance(), this
	);

	if (!hWnd)
	{
		throw WND_LASTEXCEPT();
	}

	ShowWindow(hWnd, SW_SHOWDEFAULT);

	pGraphics = std::make_unique<Graphics>(hWnd);
}

Window::~Window()
{
	DestroyWindow(hWnd);
}


void Window::SetTitle(const std::string& title)
{
	SetWindowText(hWnd, title.c_str());
}

void Window::EnableCursor() noexcept
{
	//TO BE IMPLEMENTED
}

void Window::DisableCursor() noexcept
{
	//TO BE IMPLEMENTED
}

bool Window::CursorEnabled() const noexcept
{
	//TO BE IMPLEMENTED
	return true;
}

std::optional<int> Window::ProcessMessages()
{
	MSG msg;
	while (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
	{
		if (msg.message == WM_QUIT)
		{
			return int(msg.wParam);
		}

		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return {};
}

Graphics& Window::GetGraphics()
{
	return *pGraphics;
}

LRESULT CALLBACK Window::HandleMsgSetup(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) noexcept
{
	if (msg == WM_NCCREATE)
	{
		const CREATESTRUCTW* const pCreate = reinterpret_cast<CREATESTRUCTW*>(lParam);
		Window* const pWnd = static_cast<Window*>(pCreate->lpCreateParams);

		//Save pointer to window instance in WinAPI
		SetWindowLongPtr(hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(pWnd));
		//Set message proc to new handle
		SetWindowLongPtr(hWnd, GWLP_WNDPROC, reinterpret_cast<LONG_PTR>(&Window::HandleMsgThunk));

		return pWnd->HandleMsg(hWnd, msg, wParam, lParam);
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}

LRESULT CALLBACK Window::HandleMsgThunk(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) noexcept
{
	//Setup pointer to instance and call correct handle
	Window* const pWnd = reinterpret_cast<Window*>(GetWindowLongPtr(hWnd, GWLP_USERDATA));
	return pWnd->HandleMsg(hWnd, msg, wParam, lParam);
}

LRESULT Window::HandleMsg(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) noexcept
{
	#if _DEBUG
		std::stringstream ss;
		ss << "MESSAGE( ID: " << int(msg) << ", WP: " << wParam << ", LP: " << lParam << " );" << std::endl;
		OutputDebugString(ss.str().c_str());
	#endif

	switch (msg)
	{
	case WM_CLOSE:
	{
		PostQuitMessage(0);
		return 0;	//Stop winProc from destroying window, the destructor will handle that later
	}

	/********************MANAGE KEYBOARD***********************/
	case WM_KEYDOWN:
	case WM_SYSKEYDOWN:
	{
		//Bit 30 of Lparam used to check for autorepeat
		if (!(lParam & (1 << 30)) || keyboard.IsAutorepeatEnabled())
		{
			keyboard.OnKeyPressed(static_cast<unsigned char>(wParam));
		}
		break;
	}
	case WM_KEYUP:
	case WM_SYSKEYUP:
	{
		keyboard.OnKeyReleased(static_cast<unsigned char>(wParam));
		break;
	}
	case WM_CHAR:
	{
		keyboard.OnChar(static_cast<unsigned char>(wParam));
		break;
	}
	case WM_KILLFOCUS: 
	{
		keyboard.ClearState();
		break;
	}

	/********************MANAGE MOUSE***********************/
	case WM_MOUSEMOVE:
	{
		const POINTS pt = MAKEPOINTS(lParam);
		//Handle Mouse Capture
		if (pt.x >= 0 && pt.x < width && pt.y >= 0 && pt.y < height)
		{
			mouse.OnMouseMove(pt.x, pt.y);
			if (!mouse.IsInWindow())
			{
				SetCapture( hWnd );
				mouse.OnMouseEnter();
			}
		}
		else
		{
			if (mouse.IsRightPressed() || mouse.IsLeftPressed())
			{
				mouse.OnMouseMove(pt.x, pt.y);
			}
			else
			{
				ReleaseCapture();
				mouse.OnMouseLeave();
			}
		}
		break;
	}

	case WM_LBUTTONDOWN:
	{
		const POINTS pt = MAKEPOINTS(lParam);
		mouse.OnLeftPressed(pt.x, pt.y);
		break;
	}

	case WM_LBUTTONUP:
	{
		const POINTS pt = MAKEPOINTS(lParam);
		mouse.OnLeftReleased(pt.x, pt.y);
		break;
	}

	case WM_RBUTTONDOWN:
	{
		const POINTS pt = MAKEPOINTS(lParam);
		mouse.OnRightPressed(pt.x, pt.y);
		break;
	}

	case WM_RBUTTONUP:
	{
		const POINTS pt = MAKEPOINTS(lParam);
		mouse.OnRightReleased(pt.x, pt.y);
		break;
	}

	case WM_MOUSEWHEEL:
	{
		const POINTS pt = MAKEPOINTS(lParam);
		const int delta = GET_WHEEL_DELTA_WPARAM(wParam);
		mouse.OnWheelDelta(pt.x, pt.y, delta);

		break;
	}

	}

	return DefWindowProc(hWnd, msg, wParam, lParam);
}

//####################
//Window's Exceptions
//####################

WindowException::WindowException(int line, const char* file, HRESULT hr) noexcept
: line(line), file(file), hr(hr)
{ }

char const* WindowException::what() const
{
	std::ostringstream oss;
	oss<<"Window Exception"<<std::endl
		<< "Error Code: "<< hr <<std::endl
		<< "Error String: "<< TranslateErrorCode(hr) <<std::endl
		<< "[FILE] "<<file << std::endl
		<<"[LINE] "<<line;
	whatBuffer = oss.str();
	return whatBuffer.c_str();
}

std::string WindowException::TranslateErrorCode(HRESULT hr) noexcept
{
	char* pMsgBuff = nullptr;
	DWORD nMsgLen = FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		nullptr, hr, MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US),
		reinterpret_cast<LPSTR>(&pMsgBuff), 0, nullptr
	);

	if (nMsgLen == 0) 
	{
		return "Unidentified error code";
	}

	std::string errorString = pMsgBuff;
	LocalFree(pMsgBuff);
	return errorString;
}

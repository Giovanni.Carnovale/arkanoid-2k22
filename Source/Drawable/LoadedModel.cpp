#include "Drawable/LoadedModel.h"

#include "Bindable/VertexBuffer.h"
#include "Bindable/VertexShader.h"
#include "Bindable/PixelShader.h"
#include "Bindable/ConstantBuffer.h"
#include "Bindable/InputLayout.h"
#include "Bindable/Topology.h"

#include "Graphics.h"
#include "Bindable/TransCBuff.h"

#include "Meshes/Cube.h"
#include "Vertex.h"


#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h"

namespace dx = DirectX;

LoadedModel::LoadedModel(Graphics& gfx, const dx::XMFLOAT3& position, const dx::XMFLOAT3& rotation, const dx::XMFLOAT3& scale, const dx::XMFLOAT3& materialColor, const std::string& Path)
	: LoadedModel(gfx, position, dx::XMFLOAT3(0, 0, 0), rotation, dx::XMFLOAT3(0, 0, 0), scale, materialColor, Path)
{}

LoadedModel::LoadedModel(Graphics& gfx, const dx::XMFLOAT3& position, const dx::XMFLOAT3& velocity, const dx::XMFLOAT3& rotation, const dx::XMFLOAT3& angularvelocity, const dx::XMFLOAT3& scale, const dx::XMFLOAT3& materialColor, const std::string& Path)
	: DrawableBase(), position(position), velocity(velocity), rotation(rotation),
	angularvelocity(angularvelocity), scale(scale), materialColor(materialColor), modelPath(Path)
{
	if (!IsStaticInitialized()) {
		
		VertexContainer vcont{
			std::move(
				VertexLayout()
					.Append(VertexLayout::ElementType::Position3D)
					.Append(VertexLayout::ElementType::Normal)
			)
		};

		Assimp::Importer imp;
	
		const aiScene* model = imp.ReadFile(modelPath, aiProcess_Triangulate | aiProcess_JoinIdenticalVertices);
		const aiMesh* pMesh = model->mMeshes[0];
		
		for (unsigned int i = 0; i<pMesh->mNumVertices; i++)
		{
			vcont.EmplaceBack(
				dx::XMFLOAT3{pMesh->mVertices[i].x, pMesh->mVertices[i].y, pMesh->mVertices[i].z},
				*reinterpret_cast<dx::XMFLOAT3*>(&pMesh->mNormals[i])
			);
		}

		AddStaticBind(std::make_unique<VertexBuffer>(gfx, vcont));

		auto pVertexShader = std::make_unique<VertexShader>(gfx, L"Shaders/VSPhong.cso");
		auto pByteCode = pVertexShader->GetBlob();
		AddStaticBind(std::move(pVertexShader));

		AddStaticBind(std::make_unique<PixelShader>(gfx, L"Shaders/PSPhong.cso"));


		std::vector<unsigned short> Indices;
		Indices.reserve(pMesh->mNumFaces * 3);
		for (unsigned int i = 0; i < pMesh->mNumFaces; i++)
		{
			const aiFace& face = pMesh->mFaces[i];
			Indices.push_back(face.mIndices[0]);
			Indices.push_back(face.mIndices[1]);
			Indices.push_back(face.mIndices[2]);
		}
		AddStaticIndexBuffer(std::make_unique<IndexBuffer>(gfx,Indices));

		AddStaticBind(std::make_unique<InputLayout>(gfx, vcont.GetLayout().GetD3DLayout(), pByteCode));
		AddStaticBind(std::make_unique<Topology>(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST));
	}
	else
	{
		SetIndexFromStatic();
	}

	struct PSMaterialConst
	{
		alignas(16) dx::XMFLOAT3 color = { 0.7f, 0.7f, 0.7f };
		alignas(16) dx::XMFLOAT3 diffuse = { 0.5f, .5f, .5f };
		alignas(16) dx::XMFLOAT3 specular = { .7f, .7f, .7f };
		float shininess = 1.0f;
	};

	PSMaterialConst cb;
	cb.color = materialColor;

	AddBind(std::make_unique<PixelConstantBuffer<PSMaterialConst>>(gfx, 1u, cb));

	AddBind(std::make_unique<TransCBuff>(gfx, *this));
}


DirectX::XMMATRIX LoadedModel::GetTransformXM() const noexcept
{
	return (
		DirectX::XMMatrixScaling(scale.x, scale.y, scale.z) *
		DirectX::XMMatrixRotationRollPitchYaw(rotation.x, rotation.y, rotation.z) *
		DirectX::XMMatrixTranslation(position.x, position.y, position.z)
		);
}

void LoadedModel::Update(float dt) noexcept
{
	//ugly code but wanted to try SIMD optimized types
	auto vecPosition = DirectX::XMLoadFloat3(&position);
	auto vecRotation = DirectX::XMLoadFloat3(&rotation);
	auto deltaPos = DirectX::XMVectorScale(DirectX::XMLoadFloat3(&velocity), dt);
	auto deltaRot = DirectX::XMVectorScale(DirectX::XMLoadFloat3(&angularvelocity), dt);
	vecPosition = DirectX::XMVectorAdd(vecPosition, deltaPos);
	vecRotation = DirectX::XMVectorAdd(vecRotation, deltaRot);
	DirectX::XMStoreFloat3(&position, vecPosition);
	DirectX::XMStoreFloat3(&rotation, vecRotation);
}

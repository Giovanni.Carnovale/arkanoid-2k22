#include "Drawable/Mesh.h"
#include <memory>

#include "Bindable/Topology.h"
#include "Bindable/IndexBuffer.h"
#include "Bindable/TransCBuff.h"
#include "Vertex.h"
#include "Bindable/VertexBuffer.h"
#include "Bindable/VertexShader.h"
#include "Bindable/PixelShader.h"
#include "Bindable/InputLayout.h"
#include "Bindable/ConstantBuffer.h"

Mesh::Mesh(Graphics& gfx, class std::vector<std::unique_ptr<class Bindable>> bindPtrs)
{
	DirectX::XMStoreFloat4x4(&transform, DirectX::XMMatrixIdentity());

	if (!IsStaticInitialized())
	{
		AddStaticBind(std::make_unique<Topology>(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST));
	}
	for (auto& pb : bindPtrs)
	{
		if (auto pi = dynamic_cast<IndexBuffer*>(pb.get()))
		{
			AddIndexBuffer(std::unique_ptr<IndexBuffer>(pi));
			pb.release();
		}
		else
		{
			AddBind(std::move(pb));
		}
	}

	AddBind(std::make_unique<TransCBuff>(gfx, *this));
}

void Mesh::Draw(Graphics& gfx, DirectX::FXMMATRIX accumulatedTransform) const noexcept
{
	DirectX::XMStoreFloat4x4(&transform, accumulatedTransform);
	Drawable::Draw(gfx);
}

void Mesh::Update(float dt) noexcept
{
}

DirectX::XMMATRIX Mesh::GetTransformXM() const noexcept
{
	return DirectX::XMLoadFloat4x4(&transform);
}

Node::Node(const std::string& name, std::vector<Mesh*> meshPtrs, const DirectX::XMMATRIX& transform)
	: meshPtrs(std::move(meshPtrs)), childPtrs(), name(name)
{
	DirectX::XMStoreFloat4x4(&BaseTransform, transform);
	DirectX::XMStoreFloat4x4(&AppliedTransform, DirectX::XMMatrixIdentity());
}

void Node::Draw(Graphics& gfx, DirectX::FXMMATRIX accumulatedTransform) const noexcept
{
	const auto compositeTransform = 
		DirectX::XMLoadFloat4x4(&BaseTransform) * 
		DirectX::XMLoadFloat4x4(&AppliedTransform) * 
		accumulatedTransform;

	for (const auto pMesh : meshPtrs)
	{
		pMesh->Draw(gfx, compositeTransform);
	}
	for (const auto& pChild : childPtrs)
	{
		pChild->Draw(gfx, compositeTransform);
	}
}

void Node::AddChild(std::unique_ptr<Node> pChild) noexcept
{
	assert(pChild);
	childPtrs.push_back(std::move(pChild));
}

Model::Model(Graphics& gfx, std::string filename)
{
	Assimp::Importer importer;
	const aiScene* const pScene = importer.ReadFile(filename.c_str(), aiProcess_Triangulate | aiProcess_JoinIdenticalVertices );

	for (size_t i = 0; i < pScene->mNumMeshes; i++)
	{
		meshPtrs.push_back( ParseMesh(gfx, *pScene->mMeshes[i] ) );
	}

	pRoot = ParseNode( *pScene->mRootNode);
}

void Model::Draw(Graphics& gfx, DirectX::FXMMATRIX transform) const
{
	pRoot->Draw(gfx, transform);
}

std::unique_ptr<Mesh> Model::ParseMesh(Graphics& gfx, const aiMesh& mesh)
{
	namespace dx = DirectX;

	VertexContainer vbuf(std::move(
		VertexLayout{}
		.Append(VertexLayout::ElementType::Position3D)
		.Append(VertexLayout::ElementType::Normal)
	));

	for (unsigned int i = 0; i < mesh.mNumVertices; i++)
	{
		vbuf.EmplaceBack(
			*reinterpret_cast<dx::XMFLOAT3*>(&mesh.mVertices[i]),
			*reinterpret_cast<dx::XMFLOAT3*>(&mesh.mNormals[i])
		);
	}

	std::vector<unsigned short> indices;
	indices.reserve(mesh.mNumFaces * 3);
	for (unsigned int i = 0; i < mesh.mNumFaces; i++)
	{
		const auto& face = mesh.mFaces[i];
		assert(face.mNumIndices == 3);
		indices.push_back(face.mIndices[0]);
		indices.push_back(face.mIndices[1]);
		indices.push_back(face.mIndices[2]);
	}

	std::vector<std::unique_ptr<Bindable>> bindablePtrs;

	bindablePtrs.push_back(std::make_unique<VertexBuffer>(gfx, vbuf));

	bindablePtrs.push_back(std::make_unique<IndexBuffer>(gfx, indices));

	auto pvs = std::make_unique<VertexShader>(gfx, L"Shaders/VSPhong.cso");
	auto pvsbc = pvs->GetBlob();
	bindablePtrs.push_back(std::move(pvs));

	bindablePtrs.push_back(std::make_unique<PixelShader>(gfx, L"Shaders/PSPhong.cso"));

	bindablePtrs.push_back(std::make_unique<InputLayout>(gfx, vbuf.GetLayout().GetD3DLayout(), pvsbc));

	struct PSMaterialConst
	{
		alignas(16) dx::XMFLOAT3 color = { 0.7f, 0.7f, 0.7f };
		alignas(16) dx::XMFLOAT3 diffuse = { 0.8f, .8f, .8f };
		alignas(16) dx::XMFLOAT3 specular = { .5f, .5f, .5f };
		float shininess = 1.0f;
	} pmc;

	bindablePtrs.push_back(std::make_unique<PixelConstantBuffer<PSMaterialConst>>(gfx, 1u, pmc));

	return std::make_unique<Mesh>(gfx, std::move(bindablePtrs));
}

std::unique_ptr<Node> Model::ParseNode(const aiNode& node)
{
	namespace dx = DirectX;
	const auto transform = dx::XMMatrixTranspose(
		dx::XMLoadFloat4x4(
		reinterpret_cast<const dx::XMFLOAT4X4*>(&node.mTransformation)
	)
	);

	std::vector<Mesh*> curMeshPtrs;
	curMeshPtrs.reserve(node.mNumMeshes);
	for (size_t i = 0; i < node.mNumMeshes; i++)
	{
		const auto meshIdx = node.mMeshes[i];
		curMeshPtrs.push_back(meshPtrs.at(meshIdx).get());
	}

	auto pNode = std::make_unique<Node>(std::string(node.mName.C_Str()),std::move(curMeshPtrs), transform);
	for (size_t i = 0; i < node.mNumChildren; i++)
	{
		pNode->AddChild(ParseNode(*node.mChildren[i]));
	}

	return pNode;
}

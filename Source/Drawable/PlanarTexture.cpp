#include "Drawable/PlanarTexture.h"

#include "Bindable/VertexBuffer.h"
#include "Bindable/VertexShader.h"
#include "Bindable/PixelShader.h"
#include "Bindable/Texture.h"
#include "Bindable/Sampler.h"
#include "Bindable/InputLayout.h"
#include "Bindable/Topology.h"

#include "Graphics.h"
#include "Bindable/TransCBuff.h"

#include "Meshes/Plane.h"

#include <string>
#include "Vertex.h"

namespace dx = DirectX;

PlanarTexture::PlanarTexture(Graphics& gfx, const DirectX::XMFLOAT3& position, const DirectX::XMFLOAT3& rotation, const DirectX::XMFLOAT3& scale, const std::wstring& texturepath)
	: PlanarTexture(gfx, position, DirectX::XMFLOAT3(0, 0, 0), rotation, DirectX::XMFLOAT3(0, 0, 0), scale, texturepath)
{}

PlanarTexture::PlanarTexture(Graphics& gfx, const DirectX::XMFLOAT3& position, const DirectX::XMFLOAT3& velocity, const DirectX::XMFLOAT3& rotation, const DirectX::XMFLOAT3& angularvelocity, const DirectX::XMFLOAT3& scale, const std::wstring& texturepath)
	: DrawableBase(), position(position), velocity(velocity), rotation(rotation),
	angularvelocity(angularvelocity), scale(scale)
{
	if (!IsStaticInitialized()) {

		struct Vertex
		{
			dx::XMFLOAT3 pos;
			dx::XMFLOAT2 tex;
		};

		auto model = Plane::MakeTesselated<Vertex>(1u, 1u);
		model.vertices[0].tex = {0.0f, 1.0f};
		model.vertices[1].tex = {1.0f, 1.0f};
		model.vertices[2].tex = {0.0f, 0.0f};
		model.vertices[3].tex = {1.0f, 0.0f};

		VertexContainer vcont{
			std::move(
				VertexLayout{}.Append(VertexLayout::ElementType::Position3D)
				.Append(VertexLayout::ElementType::Texture2D)
			)
		};

		for (const Vertex& v : model.vertices)
		{
			vcont.EmplaceBack(v.pos, v.tex);
		}


		AddStaticBind(std::make_unique<VertexBuffer>(gfx, vcont));

		auto pVertexShader = std::make_unique<VertexShader>(gfx, L"Shaders/VSTexture.cso");
		auto pByteCode = pVertexShader->GetBlob();
		AddStaticBind(std::move(pVertexShader));

		AddStaticBind(std::make_unique<PixelShader>(gfx, L"Shaders/PSTexture.cso"));
		AddStaticIndexBuffer(std::make_unique<IndexBuffer>(gfx, model.indices));


		AddStaticBind(std::make_unique<InputLayout>(gfx, vcont.GetLayout().GetD3DLayout(), pByteCode));
		AddStaticBind(std::make_unique<Topology>(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST));
		AddStaticBind(std::make_unique<Sampler>(gfx));
	}
	else
	{
		SetIndexFromStatic();
	}

	TextureSurface = Surface(texturepath);
	AddBind(std::make_unique<Texture>(gfx, TextureSurface));

	AddBind(std::make_unique<TransCBuff>(gfx, *this));
}


DirectX::XMMATRIX PlanarTexture::GetTransformXM() const noexcept
{
	return (
		DirectX::XMMatrixScaling(scale.x, scale.y, scale.z) *
		DirectX::XMMatrixRotationRollPitchYaw(rotation.x, rotation.y, rotation.z) *
		DirectX::XMMatrixTranslation(position.x, position.y, position.z)
		);
}

void PlanarTexture::Update(float dt) noexcept
{
	//ugly code but wanted to try SIMD optimized types
	auto vecPosition = DirectX::XMLoadFloat3(&position);
	auto vecRotation = DirectX::XMLoadFloat3(&rotation);
	auto deltaPos = DirectX::XMVectorScale(DirectX::XMLoadFloat3(&velocity), dt);
	auto deltaRot = DirectX::XMVectorScale(DirectX::XMLoadFloat3(&angularvelocity), dt);
	vecPosition = DirectX::XMVectorAdd(vecPosition, deltaPos);
	vecRotation = DirectX::XMVectorAdd(vecRotation, deltaRot);
	DirectX::XMStoreFloat3(&position, vecPosition);
	DirectX::XMStoreFloat3(&rotation, vecRotation);
}

#include "Drawable/Drawable.h"
#include <cassert>

void Drawable::Draw(Graphics& gfx) const noexcept
{
	for( auto& b : binds )
	{
		b->Bind(gfx);
	}
	for (auto& b : GetStaticBinds())
	{
		b->Bind(gfx);
	}
	gfx.DrawIndexed( pIndexBuffer->GetCount() );
}

void Drawable::AddBind(std::unique_ptr<Bindable> bind) noexcept
{
	assert( typeid(*bind) != typeid(IndexBuffer) && "Must Bind Index Buffer With AddIndexBuffer");
	binds.push_back(std::move(bind));
}

void Drawable::AddIndexBuffer(std::unique_ptr<IndexBuffer> ibuf) noexcept
{
	assert(pIndexBuffer == nullptr && "Overriding index buffer");
	pIndexBuffer = ibuf.get();
	binds.push_back(std::move(ibuf));
}


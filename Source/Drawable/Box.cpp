#include "Drawable/Box.h"

#include "Bindable/VertexBuffer.h"
#include "Bindable/VertexShader.h"
#include "Bindable/PixelShader.h"
#include "Bindable/ConstantBuffer.h"
#include "Bindable/InputLayout.h"
#include "Bindable/Topology.h"

#include "Graphics.h"
#include "Bindable/TransCBuff.h"

#include "Meshes/Cube.h"
#include "Vertex.h"

namespace dx = DirectX;

VertexContainer Box::vcont = VertexContainer(
	std::move(
		VertexLayout{}.Append(VertexLayout::ElementType::Position3D)
		.Append(VertexLayout::ElementType::Normal)
	)
);

Box::Box(Graphics& gfx, DirectX::XMFLOAT4X4& transform, const dx::XMFLOAT3& materialColor)
:	
	DrawableBase(), transform(transform), materialColor(materialColor)
{
	if (!IsStaticInitialized()) {

		struct Vertex
		{
			dx::XMFLOAT3 pos;
			dx::XMFLOAT3 normal;
		};

		auto model = Cube::MakeIndipendent<Vertex>();
		model.SetNormalsIndipendent();

		
		for (const Vertex& v : model.vertices)
		{
			vcont.EmplaceBack(
				v.pos,
				v.normal
			);
		}

		AddStaticBind(std::make_unique<VertexBuffer>(gfx, vcont));

		auto pVertexShader = std::make_unique<VertexShader>(gfx, L"Shaders/VSPhong.cso");
		auto pByteCode = pVertexShader->GetBlob();
		AddStaticBind(std::move(pVertexShader));

		AddStaticBind(std::make_unique<PixelShader>(gfx, L"Shaders/PSPhong.cso"));

		AddStaticIndexBuffer(std::make_unique<IndexBuffer>(gfx, model.indices));

		AddStaticBind(std::make_unique<InputLayout>(gfx, vcont.GetLayout().GetD3DLayout(), pByteCode));
		AddStaticBind(std::make_unique<Topology>(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST));
	}
	else
	{
		SetIndexFromStatic();
	}
	
	struct PSMaterialConst
	{
		alignas(16) dx::XMFLOAT3 color = {0.7f, 0.7f, 0.7f};
		alignas(16) dx::XMFLOAT3 diffuse = { 0.8f, 0.8f, 0.8f };
		alignas(16) dx::XMFLOAT3 specular = { 0.5f, 0.5f, 0.5f };
		float shininess = 20.0f;
	};

	PSMaterialConst cb;
	cb.color = materialColor;

	AddBind(std::make_unique<PixelConstantBuffer<PSMaterialConst>>(gfx, 1u, cb));

	AddBind(std::make_unique<TransCBuff>(gfx, *this));
}


DirectX::XMMATRIX Box::GetTransformXM() const noexcept
{
	return XMLoadFloat4x4(&transform);
}

void Box::Update(float dt) noexcept
{
	//ugly code but wanted to try SIMD optimized types

// 	auto vecPosition = DirectX::XMLoadFloat3(&position);
// 	auto vecRotation = DirectX::XMLoadFloat3(&rotation);
// 	auto deltaPos = DirectX::XMVectorScale(DirectX::XMLoadFloat3(&velocity), dt);
// 	auto deltaRot = DirectX::XMVectorScale(DirectX::XMLoadFloat3(&angularvelocity), dt);
// 	vecPosition = DirectX::XMVectorAdd(vecPosition, deltaPos);
// 	vecRotation = DirectX::XMVectorAdd(vecRotation, deltaRot);
// 	DirectX::XMStoreFloat3(&position, vecPosition);
// 	DirectX::XMStoreFloat3(&rotation, vecRotation);
}

#include "GameObj/Paddle.h"
#include "GameInfo.h"

namespace dx = DirectX;

Paddle::Paddle(Graphics& gfx, const DirectX::XMFLOAT3& position, const DirectX::XMFLOAT3& rotation, const DirectX::XMFLOAT3& scale, const DirectX::XMFLOAT3& color)
:
	GameObj(position, rotation, scale),
	PaddleMesh(gfx, transform, scale)
{}

void Paddle::Draw(class Graphics& gfx) const
{
	PaddleMesh.Draw(gfx);
}

void Paddle::Update(float dt)
{
	dx::XMFLOAT3 SpeedValues = dx::XMFLOAT3(InputAxis * Speed, 0, 0);

	DirectX::XMMATRIX DeltaMatrix = dx::XMMatrixTranslation(dt * SpeedValues.x, dt * SpeedValues.y, dt*SpeedValues.z);
	DirectX::XMMATRIX TmpMatrix = GetTransformAsMatrix();
	dx::XMStoreFloat4x4(&transform, TmpMatrix * DeltaMatrix);

	BoundsCheck();
}

void Paddle::BoundsCheck()
{
	const float x = GetPositionX();	
	const float Scale = GetScaleX();

	if (x <= GameInfo::LeftLimit || x >= GameInfo::RightLimit)
	{
		SnapToClosestHorizontalEdge();
	}
}

void Paddle::SnapToClosestHorizontalEdge()
{
	const float x = GetPositionX();

	//Closest to Left
	dx::XMVECTOR Pos, Rot, Scale;
	dx::XMMatrixDecompose(&Scale, &Rot, &Pos, GetTransformAsMatrix());
		
	if (fabs(GameInfo::LeftLimit - x) < fabs(GameInfo::RightLimit - x))
	{
		Pos = dx::XMVectorSetX(Pos, GameInfo::LeftLimit);
	} else 
	{
		Pos = dx::XMVectorSetX(Pos, GameInfo::RightLimit);
	}

	dx::XMMATRIX NewTransform = 
		dx::XMMatrixScalingFromVector(Scale) *
		dx::XMMatrixRotationRollPitchYawFromVector(Rot) *
		dx::XMMatrixTranslationFromVector(Pos);

	dx::XMStoreFloat4x4(&transform, NewTransform);
}

void Paddle::OnCollision(const Hit&)
{
}

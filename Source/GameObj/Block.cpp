#include "GameObj/Block.h"

namespace dx = DirectX;

Block::Block(Graphics& gfx, const DirectX::XMFLOAT3& position, const DirectX::XMFLOAT3& rotation, const DirectX::XMFLOAT3& scale, const DirectX::XMFLOAT3& color)
: 
	GameObj(position, rotation, scale),
	boxMesh(gfx, transform, color)
{
}

void Block::Draw(Graphics& gfx) const
{
	boxMesh.Draw(gfx);
}

void Block::Update(float dt)
{

}
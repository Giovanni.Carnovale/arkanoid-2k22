#include "GameObj/Ball.h"
#include "GameInfo.h"

namespace dx = DirectX;

Ball::Ball(Graphics& gfx, const DirectX::XMFLOAT3& position, const float radius, const float MaxSpeed)
:
	GameObj(position, dx::XMFLOAT3{0.f, 0.f, 0.f}, dx::XMFLOAT3{radius, radius, radius}),
	SphereMesh(gfx, modelPath),
	SpeedVector(dx::XMFLOAT3{1.f,1.f,0.f}),
	MaxSpeed(MaxSpeed),
	Radius(radius)
{
}

void Ball::Draw(Graphics& gfx) const
{
	SphereMesh.Draw(gfx, GetTransformAsMatrix());
}

void Ball::Update(float dt)
{

	auto speedAsVector = dx::XMLoadFloat3(&SpeedVector);
	auto normalizedVector = dx::XMVector3Normalize(speedAsVector);
	dx::XMStoreFloat3(&SpeedVector, dx::XMVectorScale(normalizedVector, MaxSpeed));

	auto deltaMatrix = dx::XMMatrixTranslation(dt * SpeedVector.x, dt * SpeedVector.y, dt * SpeedVector.z);
	auto tmpMatrix = GetTransformAsMatrix();
	dx::XMStoreFloat4x4(&transform, tmpMatrix * deltaMatrix);



	BoundsCheck();

}

void Ball::BoundsCheck()
{
	const float x = GetPositionX();
	const float y = GetPositionY();

	if (x - Radius <= GameInfo::LeftLimit)
	{
		Hit h;
		h.HitPos = dx::XMFLOAT3{ GameInfo::LeftLimit, y, 0.0f };
		h.HitNormal = dx::XMFLOAT3{ 1.0f, 0.0f, 0.0f };

		OnCollision(h);
	}
	else if (x + Radius >= GameInfo::RightLimit)
	{
		Hit h;
		h.HitPos = dx::XMFLOAT3{ GameInfo::RightLimit, y, 0.0f };
		h.HitNormal = dx::XMFLOAT3{ -1.0f, 0.0f, 0.0f };

		OnCollision(h);
	}

	if (y + Radius >= GameInfo::TopLimit)
	{
		Hit h;
		h.HitPos = dx::XMFLOAT3{ x, GameInfo::TopLimit, 0.0f };
		h.HitNormal = dx::XMFLOAT3{ 0.f, -1.0f, 0.0f };

		OnCollision(h);
	}
}

void Ball::OnCollision(const Hit& hit)
{
	const auto BallFloatPos = GetPosition();
	const auto HitNormal = dx::XMLoadFloat3(&hit.HitNormal);
	auto newSpeed = dx::XMVector3Reflect(dx::XMLoadFloat3(&SpeedVector), HitNormal);
	dx::XMStoreFloat3(&SpeedVector, newSpeed);

	//Update position 
	const auto HitPos = dx::XMLoadFloat3(&hit.HitPos);
	const auto BallPos = dx::XMLoadFloat3(&BallFloatPos);
	const auto PenetrationVector = dx::XMVectorSubtract(HitPos, BallPos);

	const float HitDistance = dx::XMVectorGetX(dx::XMVector3Length(PenetrationVector));

	const float DistanceToMove = std::max(Radius-HitDistance, 0.f);



	const auto deltaPos = dx::XMMatrixTranslationFromVector(dx::XMVectorScale(HitNormal, DistanceToMove));
	
	const auto tmpMatrix = GetTransformAsMatrix();
	dx::XMStoreFloat4x4(&transform, tmpMatrix * deltaPos);
}

#include "App.h"

#include <chrono>
#include <random>
#include <sstream>
#include "Drawable/Box.h"
#include <DirectXMath.h>
#include <exception>

#include "Surface.h"
#include "Drawable/PlanarTexture.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include "Drawable/Mesh.h"
#include "GameObj/Block.h"
#include "GameObj/Ball.h"
#include "GameObj/Paddle.h"
#include "Collision/CollisionManager.h"
#include "GameInfo.h"

namespace dx = DirectX;

App::App() : window(1280, 720, "Arkanoid 2k22"), light(window.GetGraphics()), GameOver(false) {}

int App::Go()
{	
	try
	{
		BuildBoard(8, 15);
		
			std::unique_ptr<Ball> ball = std::make_unique<Ball>(
				window.GetGraphics(), 
				dx::XMFLOAT3{0.f, -8.f, 0.f}, 
				0.4f,
				20.f
			);
		
			pBallObj = ball.get();
			gameobjects.push_back(std::move(ball));
		
			std::unique_ptr<Paddle> paddle = std::make_unique<Paddle>(
					window.GetGraphics(),
					dx::XMFLOAT3{ 0.f, -10.f, 0.f },
					dx::XMFLOAT3{0.f, 0.f, 0.f},
					dx::XMFLOAT3{4.f, 1.f, 2.f},
					dx::XMFLOAT3{1.f,0.f, 1.f}
				);
	
			pPlayerPaddle = paddle.get();
			gameobjects.push_back(std::move(paddle));
	
			window.GetGraphics().SetCameraTransform({0,0,-20.0f}, {0.0f, 0.0f, 0.0f});
	
			while (true)
			{
				if (const auto ecode = Window::ProcessMessages())
				{
					//The ecode std::optional is returned only for WIN_QUIT
					//This line is essentially what quits the game
					return *ecode;
				}
				auto frameStart = std::chrono::high_resolution_clock::now();
				DoFrame();
				auto frameEnd = std::chrono::high_resolution_clock::now();
				auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(frameEnd-frameStart).count();
				lastFrameTime = float(millis)/1000.f;
	
		#if _DEBUG
				std::stringstream ss;
				ss << "FRAME TIME: " << lastFrameTime<<"; FRAME RATE: "<< 1.f/lastFrameTime << std::endl;
				OutputDebugString(ss.str().c_str());
		#endif
			}
	}
	catch (std::exception* e)
	{
		MessageBox(NULL, e->what(), "An Error has occured!", MB_ICONERROR | MB_OK);
		return -1;
	}

}

void App::BuildBoard(int rows, int columns)
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<float> urd(-2, 2);
	std::uniform_real_distribution<float> urdColor(0.0f, 1.0f);

	const int numBoxes = rows * columns;
	const float x_offset = 2.2f, y_offset = 1.2f;
	const dx::XMFLOAT3 StartOffset = {-x_offset * (columns - 1) / 2.0f, 0.f, 0.f};
	dx::XMFLOAT3 pos = StartOffset, vel = { 0.0f, 0.0f, 0.0f }, rot = { 0,0,0 }, angvel = { 0.0f, 0.0f, 0.0f }, scale = { 2.0f, 1.f, 2.f };


	gameobjects.reserve(numBoxes);
	for (int i = 0; i < rows; i++)
	{
		const dx::XMFLOAT3 color = {urdColor(gen), urdColor(gen), urdColor(gen) };
		for (int j = 0; j < columns; j++)
		{
			auto box = std::make_unique<Block>(
				window.GetGraphics(), pos,
				rot, scale, color);
			gameobjects.push_back(std::move(box));

			pos.x += x_offset;
		}
		pos.y += y_offset;
		pos.x = StartOffset.x;
	}
	
	const float BGScale = 45.f;
	auto bg = std::make_unique<PlanarTexture>(
		window.GetGraphics(), 
		dx::XMFLOAT3{ 0.0f, 0.0f, 1.1f }, 
		dx::XMFLOAT3{0.0f, 0.0f, 0.0f},
		dx::XMFLOAT3{ BGScale, BGScale / 1.777f, 1.0f },
		L"Resources/images/background.jpg"
	);

	drawables.push_back(std::move(bg));
	light.SetPosition({0.0f, 5.0f, -5.0f});
	light.SetIntensity(5.f);
	light.SetAttenuation(5.f, 0.01f, 0.00f);
}

void App::DoFrame()
{
	if(GameOver) return;

	//Delete ready to die objects;
	for (auto It = gameobjects.begin(); It != gameobjects.end();)
	{
		if ((*It)->IsReadyToDie())
		{
			It = gameobjects.erase(It);
		}
		else
		{
			It++;
		}
	}

	window.GetGraphics().ClearBuffer(0.3f, 0.2f, 0.3f);

	{
		//===================Input Handling===================
		float InputAxis = 0.0f;
		if (window.keyboard.KeyIsPressed('A'))
		{
			InputAxis += -1.0f;
		}
		if (window.keyboard.KeyIsPressed('D'))
		{
			InputAxis += 1.0f;
		}
		pPlayerPaddle->SetInput(InputAxis);
	}

	light.Bind(window.GetGraphics());
	
	//===================Physic Step===================
	for (auto& obj : gameobjects)
	{
		obj->Update(lastFrameTime * timeScale);
	}
	
	//Check if ball is going out of the lower bound and end the game
	if (pBallObj->GetPositionY() <= GameInfo::BottomLimit)
	{
		//Game over routine
		timeScale = 0.0f;
		const float BGScale = 45.f;
		auto GOScreen = std::make_unique<PlanarTexture>(
			window.GetGraphics(),
			dx::XMFLOAT3{ 0.0f, 0.0f, -3.f },
			dx::XMFLOAT3{ 0.0f, 0.0f, 0.0f },
			dx::XMFLOAT3{ BGScale, BGScale / 1.777f, 1.0f },
			L"Resources/images/gameover.jpg"
			);
		drawables.push_back(std::move(GOScreen));
		GameOver = true;
	}

	//===================Collision Management===================
	for (auto It = gameobjects.begin(); It != gameobjects.end() - 1; It++) //One less because last is the ball
	{
		Block* BlockToCheck = static_cast<Block*>((*It).get());
		if(BlockToCheck)
			CollisionManager::CheckCollision(*pBallObj, *BlockToCheck);
	}
	CollisionManager::CheckCollision(*pBallObj, *pPlayerPaddle);

	//===================Draw Calls===================
	for (auto& obj : gameobjects)
	{
		obj->Draw(window.GetGraphics());
	}

	//Drawable can go with less precise handling
	for (auto& d : drawables)
	{
		d->Update( lastFrameTime * timeScale );
		d->Draw(window.GetGraphics());
	}

	window.GetGraphics().EndFrame();
}

cbuffer Cbuff
{
    matrix ModelView;
    matrix MVP;
};

struct VSOut
{
	float3 WorldPos : Position;
    float3 normal : Normal;
    float4 pos : SV_Position;
};

VSOut main( float3 position : POSITION , float3 n : Normal)
{
    VSOut vso;
    vso.WorldPos = (float3) mul(float4(position, 1.0f), ModelView);
    vso.normal = mul(n, (float3x3) ModelView);
    vso.normal /= length(vso.normal);
    vso.pos = mul(float4(position, 1.0f), MVP);
    return vso;
}
Texture2D tex : register(t0);

SamplerState splr;

struct VSOut
{
    float4 pos : SV_Position;
    float2 tex : TexCoord;
};

float4 main(VSOut input) : SV_TARGET
{
    return tex.Sample(splr, input.tex);

}
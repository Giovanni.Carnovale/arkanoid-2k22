cbuffer LightCBuff
{
    float InIntensity;
    float3 lightPos;
    
    float AttConst;
    float AttLin;
    float AttQuad;
    
    float3 AmbientLight;
};

cbuffer ObjCBuff
{
    float3 color;
    float3 kdiff;
    float3 kspec;
    float shininess;
};


float4 main(float3 WorldPos : Position, float3 normal : Normal) : SV_TARGET
{
    const float3 VectorToLight = lightPos - WorldPos;
    const float distance = length(VectorToLight);
    const float3 VersorToLight = VectorToLight / distance;
    float nl = dot(VersorToLight, normal / length(normal));
    nl = max(0.0f, nl);
    
    const float att = 1 / (AttConst + AttLin * distance + AttQuad * (distance * distance));
    
    float3 halfVector = (+VectorToLight - WorldPos);
    halfVector = halfVector / length(halfVector);
    const float nh = max(0.0f, dot(halfVector, normal));
    
    
    const float3 Diffuse = kdiff * nl;
    const float3 Specular = kspec * pow(nh, shininess);
    
    return float4(saturate((att * InIntensity * (Diffuse + Specular) + AmbientLight) * color), 1.0f);
}
# Arkanoid 2K22
This project is a implementation of the game __Arkanoid__. It was made as the final project for the Graphics Programming Course of a Game Dev Course. It is fully implemented using __DirectX 11__ and the __WinAPI__.


## Description
___
The project is structured can be divided in a couple of base classes from which all features are derived.

* __App__: Manages the actual game loop, simply starts the game and ticks entities;
* __Window__: Interfaces with _WinAPI_ to create the actual window and manages windows' events. Works together with _Graphics_, _Mouse_, and _Keyboard_; 
* __Graphics__: Creates and manages basic resources to interface with the _DirectX API_. If you want to draw something you should probably go through this;
* __Mouse__ and __Keyboard__: Wrappers to manage Windows events related to inputs, and to make them ease to use;
* __Bindable and derived__: Represents a single D3D11 resource (for example a vbuffer, a shader, an index buffer). It has a `Bind()` method to bind a given resource to the pipeline;
* __Drawable and derived__: Anything that you can see should be a drawable. It's made up of a set of `StaticBindables` (bindables shared between instances of the same drawable class to save up memory) and a set of per-instance Bindables;
* __GameObj__: A Gameobj is a wrapper around one or more drawables, and should contain game logic, link up collision, and so on.
* __Meshes__: A set of classes which allow procedural creation of some shapes. It is used for some models while others are loaded from .obj files
* __Vertex, VertexLayout, VertexContainer__: These three classes are used to define a vertex and the related info (example: a position + normals + color info), contain them in a single memory buffer, and be able to reuse the definition in different points of the codebase. It makes heavy use of templates. 
* __CollisionManager and Hit__: Used for collision detection, the solution is optimized for the game, using a quick Box-to-Sphere collision calculation. __Hit__ objects are used to move info about collisions.

While this composes the main part of the code, there are other classes to contain info about Cameras, Lights, Exception Handling, and so on.

### Third-party stuff

Some third-party code was included. ***The Assimp Library*** and ***The GDIPlus Library***. 
* **Assimp**: Used to load 3d models from the hard drive. It is mainly used by the **Mesh, Node and Model** classes. It also supports scene graphs, although this feature is not currently used; 
* **GDI Plus**: Allows for loading of textures in different formats. It is used initialized and destroyed by the **GDIManager** and used mainly by the **Surface and PlanarTexture** classes.

## The Demo
___
The demo presented is a simple arkanoid prototype. The game starts immediately on startup. There is only a single life so if the ball goes to the bottom of the screen it's instantly a gameover. 

### Controls

You can control the paddle with the **A** and **D** keys to move horizontally

## Installation
___
There is no special installation needed. You can open the Visual Studio file and build the game yourself.

## In the future
___
Here is a list of things I would like to work on to improve the project in the future. 

* **More general and optimized collisions system**: Currently the collision system only works in the context of this demo, but I would like to add a better system supporting space partitioning and a collider system to improve performance and usability;
* **More effects**: Currently the game only supports simple Blinn-Phong shading. In the future I would like to had shadow mapping, and other effects like outlines, more shaders, Ambient Occlusion, and so on.
* **A simple material system**: Currently you can define colors for some drawables, but the shaders already contain some hardcoded value that could be developed into a full-on materials system. 

Note that the focus of this project and any future development of it would be on the DirectX side of it and not the gameplay side.